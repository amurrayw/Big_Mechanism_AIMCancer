/*esp2model-lib.c*/

#include <stdio.h>
#include <stdlib.h>
#include "esp2model-lib.h"
#include "getline.h"
#include "string.h"

typedef struct ModelDat
{
  int InputNo;
  int OutputNo;
  char ** InputNames;
  char ** OutputNames;
}ModelDat;

void* loadFile( char * infileName, char ***dictionary, int *diclen,int*count,char ***outlines)
{

  FILE *infile;
  char *buffer;
  ModelDat *data;
  char help[1024];
  char * token;
  int temp;
  int i=0;
  help[0] = '\0';
  data = malloc(sizeof(ModelDat));
  data->InputNames = calloc(1,sizeof(char *));
  data->OutputNames = calloc(1,sizeof(char *));
  infile = fopen(infileName,"rt");
if (NULL == infile)
{
printf("Can't open %s for output.\n", infileName);
exit(EXIT_FAILURE);
}
printf("opened file\n");
while (fgetline_123(&buffer,infile))
{
  parse(dictionary, buffer, count,diclen,data);
}
fclose(infile);
printf("loaded file\n");
strcpy(help,data->InputNames[0]);
free(data->InputNames);
data->InputNames = calloc(data->InputNo,sizeof(char *));
token = strtok(help," \t");
temp = sizeof(*(data->InputNames));
fprintf(stderr, "size of char: %d, size of char *: %d, number of inputs: %d, INPUT NAMES length: %d\n",(int)sizeof(char), (int)sizeof(char *), data->InputNo,temp);
for (i=0;i<data->InputNo;i++)
{
  token = strtok(NULL," \t");
  temp = strlen(token);
  fprintf(stderr, "TOKEN length: %d\n",temp);
  data->InputNames[i] = calloc(strlen(token)+1,sizeof(char));
  strcpy(data->InputNames[i],token);
}

strcpy(help,data->OutputNames[0]);
free(data->OutputNames);
data->OutputNames = calloc(data->OutputNo,sizeof(char *));
token = strtok(help," \t");
for (i=0;i<data->OutputNo;i++)
{
  token = strtok(NULL," \t");
  data->OutputNames[i] = calloc(strlen(token)+1,sizeof(char));
  strcpy(data->OutputNames[i],token);

}
printf("copied names and everything\n");
printData(data);
fprintf(stderr, "Test1, %d\n",*count);
for (i=0;i<(*count);i++)
{
  /*fprintf(stderr, "Test, %d\n", i);*/
  printf("%s\n",(*dictionary)[i]);

}
fprintf(stderr, "Test2, done\n");
*outlines = workData(dictionary,data,count);
/*fprintf(stderr, "Test5, file name: %s, count: %d, dictionary length: %d\n", infileName, *count, *diclen);*/
printData(data);

return data;
}

void freeData(void *data)
{
  ModelDat *m = data;
  int i = 0;
  int j = 0;
  printf("freeing data\n");
  for (;i<m->InputNo;i++)
    free((m->InputNames)[i]);
  for (;j<m->OutputNo;j++)
    free((m->OutputNames)[j]); 
  free(m->InputNames);
  free(m->OutputNames);
  free(m);
}

void printData(void * data)
{
  ModelDat *m = (ModelDat*)data;
  int i = 0;
  int j =0;
  fprintf(stderr, "Test3\n");
  printf("printing data...\n");
  printf("Inputs = %d Outputs =%d \n", m->InputNo,m->OutputNo);
  for (;i<(m->InputNo);i++)
    printf("%s ",m->InputNames[i]);
  printf("\n");
  for(;j<(m->OutputNo);j++)
    printf("%s ",m->OutputNames[j]);
  printf("\n");
  fprintf(stderr, "Test4\n");
}



int parse(char ***dictionary, char *word, int * count,int*diclen,void*data)
{
  ModelDat *m = data;
  int j = 0;
	if(word[0] == '.')
  {
    if(word[1] == 'i')
    {
      m->InputNo = atoi(&(word[3]));
    }
    else if(word[1] == 'o')
    {
      if(word[2] == ' ')
      {
        m->OutputNo = atoi(&(word[3]));
      }
      else if(word[2] == 'b')
      {
        (m->OutputNames)[0] = malloc(strlen(word) + 1);
         strcpy((m->OutputNames[0]),word);
      }
    }
  if(word[1] == 'l')
    {
        (m->InputNames[0]) = malloc(strlen(word) + 1);
        strcpy((m->InputNames[0]),word);
		printf("inputs: %s\n", word);
      }
  }
  else
  {
    if (*count == *diclen)
    {
      char ** buffer;
      /* doubling dictionary if not big enough*/
      *diclen = *diclen*2;
      buffer = malloc ((*diclen)*(sizeof(char*)));
      for(;j<=*count;j++)
        buffer[j] = (*dictionary)[j];
      free(*dictionary);
      *dictionary = buffer;
     
    }
    /*fprintf(stderr, "\n\nI'm here!!!!!!\n");*/
      (*dictionary)[(*count)] = word;
      (*count)++;
     
  }
        /*for(j=0;j<*count;j++)
	  fprintf(stderr,"HERE: dictionary[%d]: %s\n",j, (*dictionary)[j]);
	    fprintf(stderr, "word: %s, count: %d, diclen: %d\n",word,*count,*diclen);*/
    return 1;
}

char **  workData(char ***dictionary,void *data,int *count)
{
  ModelDat *m = data;
  char ** outlines = calloc(m->OutputNo,sizeof(char *));
  int marker =0;
  char *inhalf;
  char *outhalf;
  char *token;
  char *input;
  char *output;
  char *final;
  char *help;
  int i, j;
  char *helpline;
  char *helper;
  char * out;
  char * in;
  int first[2];
  int temp;
  output = '\0';
  input = malloc(1);
  helpline = malloc(1);
  helper = malloc(255);
  inhalf = malloc(255);
  outhalf = malloc(255);
  help = malloc(255);
  /* initialise outlines */
  for(i=0;i<m->OutputNo;i++)
  {
    temp = strlen(m->OutputNames[i]);
    fprintf(stderr, "size: %d\n",temp);
    outlines[i] = malloc(strlen(m->OutputNames[i])+5);
    temp = sizeof((*outlines)[i]);
    fprintf(stderr, "outlines size: %d\n",temp);
    strcpy(outlines[i],m->OutputNames[i]);
    strcat(outlines[i],"* = ");
    /*fprintf(stderr, "outlines[%d]:%s\n",i,outlines[i]);*/
    first[i] = 0;
  }
    /* goes through all the markers, and changes the numbers to output */
  for(;marker<(*count);marker++)
  {
    strcpy(help,(*dictionary)[marker]);
    token = strtok(help, " \t");
    strcpy(inhalf,token);
    token = strtok(NULL," \t");
    strcpy(outhalf,token);
    out = outhalf;
    /* select the outline you are adding to */
    for (i=0;i<m->OutputNo;i++)
    {
      fprintf(stderr,"Test beginning\n");
      if (out[i] == '1')
      {
        /* adds the or in if its' not the first statement */
        if(first[i] == 0)
        {
          first[i] = 1;
        }
        else
        {
          free(helpline);
          helpline = malloc(strlen(outlines[i])+1);
          strcpy(helpline,outlines[i]);
          free(outlines[i]);
          outlines[i] = malloc(strlen(helpline)+strlen(" or ") +1);
          strcpy(outlines[i],helpline);
          strcat(outlines[i]," or ");
        }
        in = inhalf;
        input = "";
        for (j=0;j<m->InputNo;j++)
        {
          if (in[j] == '1')
          {
          strcpy(helper,input);
          /*free(input);*/ 
          input = malloc(strlen(helper)+strlen(m->InputNames[j])+6);
            if (strcmp(helper,""))
            {
              strcpy(input, helper);
              strcat(input," and ");
              strcat(input,m->InputNames[j]);
            }
            else
            {
              strcpy(input,"(");
              strcat(input,m->InputNames[j]);
            }
          }
          else if (in[j] == '0')
          {
          strcpy(helper,input);
          /*free(input);*/
          input = malloc(strlen(helper) + strlen(m->InputNames[j])+10);
          if (strcmp(helper,""))
          {
            strcpy(input,helper);
            strcat(input," and not ");
            strcat(input,m->InputNames[j]);
          }
         else
         {
          strcpy(input,"(not ");
          strcat(input,m->InputNames[j]);
         }
	  } 
	 

	}
	/*fprintf(stderr,"Test here1\n");*/
        /* input now contains the names of the particular inputs  */
    final = malloc(strlen(outlines[i])+strlen(input)+ 3);
    temp = (strlen(outlines[i])+strlen(input)+ 3);
    fprintf(stderr, "Test size: %d\n", temp);
    strcpy(final,outlines[i]);
    strcat(final,input);
    strcat(final,")");
    strcat(final,"\0");
    printf("%s\n",final);
    /*free(outlines[i]);*/
    free(input);
    free(output);
    outlines[i] = final;
   
      }
  fprintf(stderr, "final: %s\n", outlines[i]);
    }
 fprintf(stderr,"Test end\n");
  }
  free(helper); 
  free(inhalf);
  free(outhalf);
  free(help); 
  free(helpline);
 
  for (i=0; i<m->OutputNo;i++){
    strcat(outlines[i],"\0");
    fprintf(stderr, "output[%d]:  %s\n",i,outlines[i]);
  }
  return outlines;
}

void printFile( char **outlines, FILE *stream,void *data)
{
  ModelDat *m = data;
  int marker =0;
  int i;
  char *temp_name;
  
  
  temp_name = malloc(strlen(m->OutputNames[1]));
  strncpy(temp_name, m->OutputNames[1], strlen(m->OutputNames[1]));
  temp_name[strlen(m->OutputNames[1])] = '\0';
 
  fprintf(stream,"Output = %s\n",temp_name);
  fprintf(stream,"Inputs =");
  for (i=0;i<m->InputNo;i++)
  {
    if (i%2 == 1){
	  temp_name = malloc(strlen(m->InputNames[i]));
	  strncpy(temp_name, m->InputNames[i], strlen(m->InputNames[i]));
	  temp_name[strlen(m->InputNames[i])] = '\0';
      fprintf(stream," %s",temp_name);
	}
  }
  fprintf(stream,"\n");
  while (marker < m->OutputNo){
    fprintf(stream,"%s\n",outlines[marker]);
    marker ++;
  }
}
