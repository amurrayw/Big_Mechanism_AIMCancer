#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "esp2model-lib.h"
#include "getline.h"

#define true 1
#define false 0

typedef struct ModelDat
{
  int InputNo;
  int OutputNo;
  char **InputNames;
  char **OutputNames;
}ModelDat;

int main(int argc, char *argv[])
{
  ModelDat *data;
  FILE *outfile;
  int count = 0;
  int diclen = 50;
  char **dictionary = calloc(diclen,(sizeof(char *)));
  int i = 0;
  int k = 0;
  char **outlines;
  for(;k<diclen;k++)
    dictionary[k] = "\0";

  if (argc <3)
  {
    printf("usage: ./%s <infileName> <outFilename>\n",argv[0]);
    return EXIT_FAILURE;
  }
  fprintf(stderr, "Test5\n");
  fprintf(stderr, "Test6,%s\n", argv[1]);
  data = loadFile(argv[1],&dictionary, &diclen, &count,&outlines);
  /*fprintf(stderr, "Test6 0,%s\n", argv[1]);*/
  fprintf(stderr, "Test6 1,%d\n", diclen);
  fprintf(stderr, "Test6 2,%d\n", count);
  /*printf("Finished load of %s\n",argv[1]);*/
  fprintf(stderr, "Test6 2\n");
  
  /*opening the file */  
  outfile = fopen(argv[2],"wt");
 fprintf(stderr, "Test6 3\n");
  if (NULL == outfile)
  {
    printf("Can't open %s for output.\n",argv[2]);
    exit(EXIT_FAILURE);
  }
  printFile(outlines, outfile,data);
  for(i=0;i<data->OutputNo;i++)
  {
    free(outlines[i]);
  }
  free(outlines);
  freeData(data);
  fclose(outfile); 
  for (i=0;i<count;i++)
    free(dictionary[i]);
  free(dictionary);
  printf("success\n");
  return (EXIT_SUCCESS);
}
