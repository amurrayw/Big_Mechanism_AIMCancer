#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/*
   This file is used to modify the txt files from Peter's output in step 6b
   The changes are as follows:
   Rules from _1.txt - name_0 -> not (name_1 or name_2), name_1 -> name, name_0_x -> not (name_1_x or name_2_x)
   Rules from _2.txt - name_x_1 -> name_x_D1, name_x_2 -> name_x_D2, name_x_3 -> name_x_D3
   Rules from _3.txt - name_x_final -> name_x
 */



void removeString(char* string, int startIndex, int shiftAmount);
int contains(char* source, char* lookup);
char* getName(char* source, int endIndex);
int containsWithLetterBefore(char* source, char* lookup);
int posMinimum(int num1, int num2);
void insertD(char* string, int index);
int containsWithNumberBefore(char* source, char* lookup);

int main(int argc, char* argv[]) {
    if (argc > 2) {
	printf("Incorrect number of arguments specified: argc\n");
	return -1;
    }

    //Get information about the filename input
    char* fileName = argv[1];
    FILE *oldFile = fopen(fileName, "r");
    char* regulatorName = strtok(fileName, "_");
    char* numberTXT_string = strtok(NULL, ".txt");
    int numberTXT = atoi(numberTXT_string);

    //Create new File to write to called: name_x_final.txt
    char* newFileName = malloc(sizeof(char) * 4096);
    strcpy(newFileName, regulatorName);
    strcat(newFileName, "_");
    strcat(newFileName, numberTXT_string);
    strcat(newFileName, "_fixed.txt");
    FILE *newFile = fopen(newFileName, "w");

    //Loop to remove "_final" everywhere
    char* currLine = malloc(8192 * sizeof(char));     //QUESTION: WILL THIS BE LARGE ENOUGH?
    if (numberTXT == 3) {
	while (fscanf(oldFile, "%[^\n]\n", currLine) != EOF) {
	    char lookup[12];
	    sprintf(lookup, "_final");         //"_final" is the string we are going to remove
	    int index = contains(currLine, lookup);
	    while (index != -1) {
		removeString(currLine, index, strlen(lookup));
		index = contains(currLine, lookup);
	    }
	    fprintf(newFile, "%s\n", currLine);
	}
    }
    else if (numberTXT == 2) {
	while (fscanf(oldFile, "%[^\n]\n", currLine) != EOF) {
	    char lookup1[12];
            char lookup1PAREN[12];
            sprintf(lookup1, "_1 ");
            sprintf(lookup1PAREN, "_1)");
            int index1WITHOUTPAREN = containsWithNumberBefore(currLine, lookup1);
            int index1WITHPAREN = containsWithNumberBefore(currLine, lookup1PAREN);
            int index1 = posMinimum(index1WITHOUTPAREN, index1WITHPAREN);
            while (index1 != -1) {
                insertD(currLine, index1 + (1*sizeof(char)));
                index1 = containsWithNumberBefore(currLine, lookup1);
            }
	    fprintf(newFile, "%s\n", currLine);
	}
    }
    else if (numberTXT == 1) {
	while (fscanf(oldFile, "%[^\n]\n", currLine) != EOF) {
	    // printf("CURRLINE: %s\n", currLine);
	    char currLineStart[24];
	    int size = 0;
	    //Moves the cursor passed the equals sign
	    while (currLine[0] != '=') {
		size++;
		currLine = currLine + (1 * sizeof(char));
	    }
	    currLine = currLine - (size * sizeof(char));
	    strncpy(currLineStart, currLine, size);
	    currLineStart[size] = '\0';
	    currLine = currLine + (size * sizeof(char));
	    fprintf(newFile, "%s", currLineStart);
	    int newSize = 0;
	    while (!(currLine[0] != '(' && currLine[0] != ' ')) {
		newSize++;
		currLine = currLine + (1 * sizeof(char));
	    }
	    currLine = currLine - ((newSize)*sizeof(char));
	    if (currLine[0] == '(') newSize++;
//	    printf("%s\n", currLineStart);

            //name_1 changes
            char lookup1[12];
            char lookup1PAREN[12];
            sprintf(lookup1, "_1 ");
            sprintf(lookup1PAREN, "_1)");
            int index1WITHOUTPAREN = containsWithLetterBefore(currLine, lookup1);
            int index1WITHPAREN = containsWithLetterBefore(currLine, lookup1PAREN);
            int index1 = posMinimum(index1WITHOUTPAREN, index1WITHPAREN);
            while (index1 != -1) {
                removeString(currLine, index1, 2);
                index1 = containsWithLetterBefore(currLine, lookup1);
            }


            //name_0 changes
	    char lookup0[12];
	    char lookup0PAREN[12];
	    sprintf(lookup0, "_0 ");
	    sprintf(lookup0, "_0)");
	    int index0NOPAREN = containsWithLetterBefore(currLine, lookup0);
	    int index0WITHPAREN = containsWithLetterBefore(currLine, lookup0PAREN);
	    int index0 = posMinimum(index0NOPAREN, index0WITHPAREN);
	    while (index0 != -1) {
		char* name = getName(currLine, index0-1);
		removeString(currLine, index0-strlen(name), strlen(name)+2);
		int startIndex = index0 - strlen(name);
		char* newLine = malloc(sizeof(char) * 8192);
		strncpy(newLine, currLine, startIndex);
		//printf("%s\n", newLine);
		char newPhrase[8192];
		sprintf(newPhrase, " not (%s_1 or %s_2) ", name, name);
		strcat(newLine, newPhrase);
		//printf("%s\n", newLine);
		strcat(newLine, currLine + ((startIndex) * sizeof(char)));
		//printf("%s\n", newLine);
		currLine = newLine;
		index0 = containsWithLetterBefore(currLine, lookup0);
	    }



	    //name_0_x changes
	    char lookup[12];
	    sprintf(lookup, "_0_");           //"_0_" is the string we are looking for
	    int index = contains(currLine, lookup);
	    while (index != -1) {
		char num_string[3];
		strncpy(num_string, currLine + ((index+3)*sizeof(char)), 1);
		int num = atoi(num_string);
//		printf("NUM: %d\n", num);
		char* name = getName(currLine, index-1);
//		printf("NAME: %s\n", name);
		removeString(currLine, index - strlen(name), strlen(name)+4);
		int startIndex = index - strlen(name);
		char* newLine = malloc(sizeof(char) * 8192);
		strncpy(newLine, currLine, startIndex);
//		printf("%s", currLineStart);
//		printf("%s\n", newLine);
		char newPhrase[8192];
		sprintf(newPhrase, " not (%s_1_%d or %s_2_%d) ", name, num, name, num);
//		printf("NEW PHRASE: %s\n", newPhrase);
		strcat(newLine, newPhrase);
//		printf("%s", currLineStart);
//		printf("%s\n", newLine);
		strcat(newLine, currLine + (((startIndex) * sizeof(char))));
//		printf("%s", currLineStart);
//              printf("%s\n", newLine);
//		printf("\n\n");
		currLine = newLine;
		index = contains(currLine, lookup);
	    }


	    //Getting rid of unecessary spaces
            char lookupSPACE[12];
            sprintf(lookupSPACE, "  ");
            int indexSPACE = contains(currLine, lookupSPACE);
            while (indexSPACE != -1) {
                removeString(currLine, indexSPACE, 1);
                indexSPACE = contains(currLine, lookupSPACE);
            }

	    fprintf(newFile, "%s\n", currLine);
	}
    }


    //Close the files we opened
    if (fclose(oldFile) != 0) printf("OLDFILE NOT CLOSE PROPERLY\n\n\n");
    if (fclose(newFile) != 0) printf("NEWFILE NOT CLOSE PROPERLY\n\n\n\n");

    return 1;
}

//takes a string, startIndex (inclusive), and length to be removed and removes it from the string
void removeString(char* string, int startIndex, int shiftAmount) {
    int originalLength = strlen(string);
    for (int i = startIndex; (i+shiftAmount) <= strlen(string); i++) {
	if ((i + shiftAmount) < strlen(string)) string[i] = string[i+shiftAmount];
	else string[originalLength-shiftAmount] = '\0';
    }
}

//returns the index of the start of the lookup string if it is contained in the source string and returns -1 otherwise
int contains(char* source, char* lookup) {
    int lookupIndex = 0;
    int startIndex = -1;
    for (int i = 0; i < strlen(source); i++) {
	if (source[i] == lookup[lookupIndex]) {
	    if (lookupIndex == 0) {
		startIndex = i;
	    }
	    lookupIndex++;
	}
	else {
	    lookupIndex = 0;
	    startIndex = -1;
	}
	if (lookupIndex == strlen(lookup)) return startIndex;
    }
    return -1;
}

//gets the element name given, the index of the underscore where the name starts, keeps iterating backwards until it hits a space of '('
char* getName(char* source, int endIndex) {
    char* regulator = malloc(sizeof(char) * 12);
    int theEnd = endIndex;
    endIndex = endIndex - 1;
    while((source[endIndex] != '(') && (source[endIndex] != ' ')) {
	endIndex = endIndex - 1;
    }
    endIndex = endIndex + 1;
    strncpy(regulator, source + (endIndex * sizeof(char)), theEnd - endIndex+1);
    regulator[theEnd-endIndex+1] = '\0';
    return regulator;
}

char* addString(char* dest, char* source, int index) {
    for (int i = strlen(dest); i >= index; i--) {
	dest[i+strlen(source)] = dest[i];
    }
    for (int j = 0; j < strlen(source); j++) {
	dest[j+index] = source[j];
    }
}

//finds the index of the specified string, and ensures that there is a letter before it not '_' or number
int containsWithLetterBefore(char* source, char* lookup) {
    int lookupIndex = 0;
    int startIndex = -1;
    for (int i = 0; i < strlen(source); i++) {
	if (source[i] == lookup[lookupIndex]) {
	    if (lookupIndex == 0) {
		startIndex = i;
	    }
	    lookupIndex++;
	}
	else {
	    lookupIndex = 0;
	    startIndex = -1;
	}
	if (lookupIndex == strlen(lookup) && (source[startIndex-1] < 123 && source[startIndex-1] > 64)) return startIndex;
    }
    return -1;
}

//returns the minimum of the two numbers, where -1 does not count as being the smallest
int posMinimum(int num1, int num2) {
    if (num1 == -1) return num2;
    if (num2 == -1) return num1;
    else if (num1 < num2) return num1;
    else if (num2 < num1) return num2;
    else return num1;
}

//inserts a capital D at the specified index
void insertD(char* string, int index) {
    for (int i = strlen(string); i >= index; i--) {
	string[i+1] = string[i];
    }
    string[index] = 'D';
}

int containsWithNumberBefore(char* source, char* lookup) {
    int lookupIndex = 0;
    int startIndex = -1;
    for (int i = 0; i < strlen(source); i++) {
	if (source[i] == lookup[lookupIndex]) {
	    if (lookupIndex == 0) {
		startIndex = i;
	    }
	    lookupIndex++;
	}
	else {
	    lookupIndex = 0;
	    startIndex = -1;
	}
	if (lookupIndex == strlen(lookup) && (source[startIndex-1] < 58 && source[startIndex-1] > 47)) return startIndex;
    }
    return -1;
}
