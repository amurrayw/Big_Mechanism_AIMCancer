#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

int main(int argc, char* argv[]) {
    if (argc > 2) {
	printf("Incorrect number of arguments specified: %d\n", argc);
	return -1;
    }

    //Get information about the filename input
    char *fileName = argv[1];
    char *argv1[] = {"./model", fileName, NULL};
    printf("File Name: %s\n", fileName);
    pid_t pid = fork();
    if (pid == 0) execv("./model", argv1);
    else {
	waitpid(pid,0,0);
	printf("Ran Model\n");
	char *regulatorName = strtok(fileName, ".");
	char newName[strlen(regulatorName)+6];
	strcat(regulatorName, "_0.pla");
	strcat(newName, "_0.txt");
	char *argv2[] = {"./esp2model", regulatorName, newName, NULL};
//	pid = fork();
//	if (pid == 0) execv("./esp2model", argv2);
//	else {
//	    printf("Ran esp_0\n");
	    strtok(regulatorName, "_");
	    strtok(newName, "_");
	    strcat(regulatorName, "_1.pla");
	    strcat(newName, "_1.txt");
	    char *argv3[] = {"./esp2model", regulatorName, newName, NULL};
	    pid = fork();
	    if (pid == 0) execv("./esp2model", argv3);
	    else {
		printf("Ran esp_1\n");
		strtok(regulatorName, "_");
		strtok(newName, "_");
		strcat(regulatorName, "_2.pla");
		strcat(newName, "_2.txt");
		char *argv4[] = {"./esp2model", regulatorName, NULL};
//		pid = fork();
//		if (pid == 0) execv("./esp2model", argv4);
//		else {
//		printf("Ran esp_2\n");
		strtok(regulatorName, "_");
		strtok(newName, "_");
		strcat(regulatorName, "_3.pla");
		strcat(newName, "_3.txt");
		char *argv5[] = {"./esp2model", regulatorName, newName, NULL};
		pid = fork();
		if (pid == 0) execv("./esp2model", argv5);
		else {
		    printf("Ran esp_3\n");
		    strtok(regulatorName, "_");
		    strcat(regulatorName, "_0.txt");
		    char *argv2[] = {"./filterNames", regulatorName, NULL};
//		    pid = fork();
//		    if (pid == 0) execv("./filterNames", argv2);
//		    else {
//			printf("Ran NX_0\n");
			strtok(regulatorName, "_");
			strcat(regulatorName, "_1.txt");
			printf("NX_1 : %s\n", regulatorName);
			char *argv3[] = {"./filterNames", regulatorName, NULL};
			pid = fork();
			if (pid == 0) execv("./filterNames", argv3);
			else {
			printf("Ran NX_1\n");
			    strtok(regulatorName, "_");
			    strcat(regulatorName, "_2.txt");
			    char *argv4[] = {"./filterNames", regulatorName, NULL};
//			    pid = fork();
//			    if (pid == 0) execv("./filterNames", argv4);
//			    else {
//				printf("Ran NX_2\n");
				strtok(regulatorName, "_");
				strcat(regulatorName, "_3.txt");
				char *argv5[] = {"./filterNames", regulatorName, NULL};
				pid = fork();
				if (pid == 0) execv("./filterNames", argv5);
				else {
				    printf("Ran NX_3\n");
				}
				//}
			}
			//}
		}
		//}
		}
    }
    return 1;
}
