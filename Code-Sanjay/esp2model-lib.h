/* esp2model-lib.h */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void freeData(void *data);
void printData(void * data);
int parse(char ***dictionary, char*word, int *count,int*diclen,void *data);
void printFile(char **outlines, FILE *stream,void *data);
void* loadFile(char *infileName, char ***dictionary, int *diclen,int*count, char ***outlines);
char ** workData(char ***dictionary,void *data,int*count);
