#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "test_cases.h"
#include "parsing.c"
//#define tests
//#define debug
/*struct stren {
    int strength;
    int delay;
};*/
void model(int *arrA, int *arrB, int arrSize, int maxDelay, char **vars, int *poss,
           int molNums, int systemMax, FILE* fp0, FILE* fp1, FILE* fp2, FILE* fp3);
int maxDelay(int* arrB, int arrSize);
void printRules(char ***rules, int num, char* str, int systemMax);
void printesp(int **esp, int realsum, int realsumwoffset, int arrSize, FILE* fp);
int** truthTables(int sum, int arrSize, int limit, int *poss, int *arrA, int *arrB, int maxDelay, int realsum);
int sumPoss(int *poss, int molNums);
void printPreRules(char ***rules, int num, int maxDelay, int arrSize, int *arrB, int *arrA, char **vars, int limit, int *poss);
void printheader0(char *varname, int stren, FILE* fp);
void printheader1(char **vars, int limit, int *poss, int arrSize, int delay, int rs, FILE* fp);
void printheader2(char *varname, int stren, int delay, FILE* fp);
void printheader3(char* varname, int stren, FILE* fp);
int max(int a, int b) {
    if (a < b)
        return b;
    else
        return a;
}
int n = 5;

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("usage: %s filename", argv[0]);
    }
    int i;
    struct table* test = p(argv[1]);
    int MD, molNum;

////////////////////////////////////
//Open new file to write to
    char* fname0 = malloc(sizeof(char) * 1024);
    char* fname1 = malloc(sizeof(char) * 1024);
    char* fname2 = malloc(sizeof(char) * 1024);
    char* fname3 = malloc(sizeof(char) * 1024);
    strcpy(fname0, strtok(argv[1], "."));
    strcpy(fname1, strtok(argv[1], "."));
    strcpy(fname2, strtok(argv[1], "."));
    strcpy(fname3, strtok(argv[1], "."));

    strcat(fname0, "_0.pla");
    strcat(fname1, "_1.pla");
    strcat(fname2, "_2.pla");
    strcat(fname3, "_3.pla");
    FILE* fp0 = fopen(fname0, "w");
    FILE* fp1 = fopen(fname1, "w");
    FILE* fp2 = fopen(fname2, "w");
    FILE* fp3 = fopen(fname3, "w");

////////////////////////////////////
/*#ifdef tests
    tables* t = tests();
#endif*/

    model(test -> arrA, test -> arrB, test -> arrSize, max(1,maxDelay(test -> arrB, test -> arrSize)),
          test -> vars, test -> poss, test -> n, 3, fp0, fp1, fp2, fp3);

#ifdef tests
    for (i = 0; i < 3; i++) {
        char** vars = t[i] -> vars;
        int* poss = t[i] -> poss;
        int* arrA = t[i] -> arrA;
        int* arrB = t[i] -> arrB;
        int n = t[i] -> n;
        int arrSize = t[i] -> arrSize;
        MD = maxDelay(arrB, arrSize);
        molNum = n;
        fprintf(fp,"\n\n////////////////////////////////////////////////////\n");
        fprintf(fp,"test number %d, molecule %s, %d inputs\n", i, vars[n - 1], n - 1);
        fprintf(fp,"////////////////////////////////////////////////////\n");
        model(arrA, arrB, arrSize, MD, vars, poss, molNum, 3);
    }
#endif

    //molecule strength array, delay array, maximum delay given, molecule variable names, possible strengths, number of vars
}

void model(int *arrA, int *arrB, int arrSize, int maxDelay, char **vars, int *poss,
           int molNums, int systemMax, FILE* fp0, FILE* fp1, FILE* fp2, FILE* fp3) {
    //p();
    int i, j, k;
    int numDelays = maxDelay;
    int limit = molNums - 1;
//    int delay = maxDelay(arrB, arrSize);
    int num = poss[limit];
    //set delay trackers

    char*** rules = malloc(sizeof(char**) * num); //strings of rules
    for (i = 0; i < num; i++) {
        rules[i] = malloc(sizeof(char *) * maxDelay + 1);
    }
    for (i = 0; i < num; i++) {
        for (j = 0; j < maxDelay + 1; j++) {
            rules[i][j] = malloc(sizeof(char) * 10000);
            strcpy(rules[i][j], "");
        }
    }

#ifdef debug
    printPreRules(rules, num, maxDelay, arrSize, arrB, arrA, vars, limit, poss);
    printRules(rules, num, vars[limit], systemMax);
#endif
    int sum = sumPoss(poss, molNums);
    int realsum = (sum - poss[limit]) * (maxDelay + 1) + poss[limit];
    int realsumwoffset = realsum - poss[limit];
#ifdef debug
    printf("sum: %d, offset: %d, maxDelay: %d, realsum: %d\n", sum, poss[limit], maxDelay, realsum);
#endif
    int** esp = truthTables(sum, arrSize, limit, poss, arrA, arrB, maxDelay, realsum);
    printheader0(vars[limit], poss[limit], fp0);
    printheader1(vars, limit, poss, arrSize, maxDelay, realsumwoffset, fp1);
    printheader2(vars[limit], poss[limit], systemMax, fp2);
    printheader3(vars[limit], poss[limit], fp3);
    printesp(esp, realsum, realsumwoffset, arrSize, fp1);
    fprintf(fp1,"\n.e\n");

    free(rules);

}

void printheader3(char* varname, int stren, FILE* fp) {
    int i, j;
    fprintf(fp, ".lib");
    for (i = 0; i < stren; i++) {
        fprintf(fp, " %s_%d_tmp", varname, i);
    }
    for (j = 0; j < stren; j++) {
        fprintf(fp, " %s_%d", varname, j);
    }
    fprintf(fp, "\n.i %d\n", stren * 2);
    fprintf(fp, ".o %d\n", stren);
    fprintf(fp, ".ob");
    for (j = 0; j < stren; j++) {
        fprintf(fp, " %s_%d_final", varname, j);
    }
    fprintf(fp, "\n");
    fprintf(fp, ".p %d\n", stren * 2);

    for (i = 0; i < stren; i++) {
        for (j = 0; j < stren; j++) {
            fprintf(fp, "0");
        }
        for (j = 0; j < stren; j++) {
            if (j == i)
                fprintf(fp, "1");
            else
                fprintf(fp, "-");
        }
        fprintf(fp, "     ");
        for (j = 0; j < stren; j++) {
            if (j == i)
                fprintf(fp, "1");
            else
                fprintf(fp, "0");
        }
        fprintf(fp, "\n");
        for (j = 0; j < stren*2; j++) {
            if (j == i)
                fprintf(fp, "1");
            else
                fprintf(fp, "-");
        }
        fprintf(fp, "     ");
        for (j = 0; j < stren; j++) {
            if (j == i)
                fprintf(fp, "1");
            else
                fprintf(fp, "0");
        }
        fprintf(fp, "\n");
    }
    fprintf(fp, ".e\n");
}

void printheader2(char *varname, int stren, int delay, FILE* fp) {
    for (int i = 0; i < stren; i++){
        for (int j = 0; j < delay; j++) {
            fprintf(fp, ".lib ");
            if (j == 0) {
                fprintf(fp, "%s_%d\n", varname, i);
            }
            else {
                fprintf(fp, "%s_%d_%d\n", varname, i, j);
            }
            fprintf(fp, ".i 1\n");
            fprintf(fp, ".o 1\n");
            fprintf(fp, ".ob %s_%d_%d\n", varname, i, j+1);
            fprintf(fp, "p 1\n");
            fprintf(fp, "1     1\n");
            fprintf(fp, ".e\n\n");
        }
    }
}
void printheader0(char *varname, int stren, FILE* fp) {
    for (int i = 0; i < stren; i++) {
        fprintf(fp, ".lib %s_%d_final\n", varname, i);
        fprintf(fp, ".i 1\n");
        fprintf(fp, ".o 1\n");
        fprintf(fp, ".ob %s_%d\n", varname, i);
        fprintf(fp, ".p 1\n");
        fprintf(fp, "1     1\n");
        fprintf(fp, ".e\n\n");
    }
}

void printheader1(char **vars, int limit, int *poss, int arrSize, int delay, int rs, FILE* fp) {
    fprintf(fp,".lib");
    int i, j, k, stren;
    char* str;
    for (i = limit - 1; i >= 0; i--) {
        stren = poss[i];
        str = vars[i];
        for (j = 0; j < stren; j++) {
            for (k = 0; k < delay + 1; k++) {
                if (k == 0) {
                    fprintf(fp," %s_%d", str, j);
                }
                else {
                    fprintf(fp," %s_%d_%d", str, j, k);
                }
            }
        }
    }
    fprintf(fp,"\n.i %d\n", rs);
    fprintf(fp,".o %d\n", poss[limit]);
    fprintf(fp,".ob ");
    for (i = 0; i < poss[limit]; i++) {
        fprintf(fp," %s_%d_tmp", vars[limit], i);
    }
    fprintf(fp,"\n.p %d", arrSize);
}

void printPreRules(char ***rules, int num, int maxDelay, int arrSize, int *arrB, int *arrA, char **vars, int limit, int *poss) {
    int i, j, k;
    int track, del;
    int mod, len;
    char* str;
    char format[200];
    for (j = 0; j < arrSize; j++) {
        track = j;
        del = arrB[j];
        strcat(rules[arrA[j]][0], "(");

        for (i = limit - 1; i >= 1; i--) {
            mod = track % poss[i];
            for (k = 0; k < del + 1; k++) {

                str = vars[i];
                if (k != 0) {
                    sprintf(format, "%s_%d_del%d  and  ", str, mod, k);
                }
                else {
                    sprintf(format, "%s_%d  and  ", str, mod);
                }
                strcat(rules[arrA[j]][0], format);
                track = track/poss[i];
            }
        }
        mod = track % poss[0];
        str = vars[0];
        sprintf(format, "%s_%d)", str, mod);
        strcat(rules[arrA[j]][0], format);
        track = track/poss[0];

        strcat(rules[arrA[j]][0], " or\n");
    }
    for (k = 0; k < num; k++) {
        len = strlen(rules[k][0]);
        strcpy((rules[k][0] + len - 4), "\0");
    }

}

void printesp(int **esp, int realsum, int realsumwoffset, int arrSize, FILE* fp) {
    int i, j;
    for (i = 0; i < arrSize; i++) {
        for (j = 0; j < realsum; j++) {
            if (j == realsumwoffset) {
                fprintf(fp,"    ");
            }
            else if (j == 0) {
                fprintf(fp,"\n");
            }
            if (j < realsumwoffset) {
                if (esp[i][j] == 1)
                    fprintf(fp,"%d", esp[i][j]);
                else
                    fprintf(fp,"-");
            }
            else
                fprintf(fp, "%d", esp[i][j]);
        }
    }
}

int** truthTables(int sum, int arrSize, int limit, int *poss, int *arrA, int *arrB, int maxDelay, int realsum) {
    int **A = calloc(sizeof(int *), arrSize);
    int i, j, k, mod;
    int offset, track, index, strength;
    //int realsum = (sum - poss[limit]) * (maxDelay + 1) + poss[limit];
    for (i = 0; i < arrSize; i++) {
        A[i] = calloc(sizeof(int), realsum);
    }
    for (j = 0; j < arrSize; j++) {
        offset = 0;
        track = j;
        int del = arrB[j];
        //fprintf(fp,"index for j = %d\n", j);
        for (i = limit - 1; i >= 0; i--) {
            mod = track % poss[i];
            for (k = 0; k < del + 1; k++) {
                //fprintf(fp,"mod = %d, poss[i] = %d\n", mod, poss[i]);
                index = offset + mod*(maxDelay + 1) + k;
                //fprintf(fp,"%d\n", index);
                A[j][index] = 1;
                track = track/poss[i];
            }
            offset += poss[i] * (maxDelay + 1);
        }
        strength = arrA[j];
        A[j][offset + strength] = 1;
    }
    return A;
}


int sumPoss(int *poss, int molNums) {
    int i;
    int sum = 0;
    for (i = 0; i < molNums; i++) {
        sum += poss[i];
    }
    return sum;
}

void printRules(char ***rules, int num, char* str, int systemMax) {
    int i, j;
    char *finals = malloc(sizeof(char) * 10000);

    for (i = 0; i < num; i++) {
        printf("%s_%d = %s_%d_final\n", str, i, str, i);
            printf("Here!\n");

    }
    for (i = 0; i < num; i++) {
        printf("%s", str);
        printf("_%d_tmp = \n", i);
        printf("%s\n", rules[i][0]);
    }
    for (i = 0; i < num; i++) {
        for (j = 1; j < systemMax + 1; j++) {
            if (j != 1) {
                printf("%s_%d_del%d = %s_%d_del%d\n", str, i, j, str, i, j - 1);
            }
            else {
                printf("%s_%d_del%d = %s_%d\n", str, i, j, str, i);
            }
        }
//        free(rules[i]);
    }
    for (i = 0; i < num; i++) {
        printf("%s_%d_final =\n(", str, i);
        for (j = 0; j < num; j++) {
            printf("~%s_%d_tmp & ", str, j);
        }
        printf("%s)", str);
        printf(" & %s_%d_tmp\n\n", str, i);
    }

}

int maxDelay(int* arrB, int arrSize) {
    int i;
    int max = 0;
    for (i = 0; i < arrSize; i++) {
        if (arrB[i] > max) {
            max = arrB[i];
        }
    }
    return max;
}
