# Big_Mechanism_AIMCancer

This is the analysis pipeline for the AIMCancer group, within the FRIES consortium, of the DARPA 'Big Mechanism' project.

It will contain modules for merging biological models, simulating models, and translating between different model types.

## Pipeline

This flow chart shows the current state of the pipeline:

![Pipeline](doc/images/pipeline.png)

I suggest we use something like the following directory structure as a starting point when we migrate our code to this repository:

![Directory structure](doc/images/directory_structure.png)