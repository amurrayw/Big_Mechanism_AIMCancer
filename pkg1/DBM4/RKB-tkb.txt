(entry tval(qidlist tkb RKB)(notes(note graph :=(qval RKB))
(note source :=(qval rules QQQ))
(note unreachable :=(qval)))(tval QQQ pnTrans('12rjr.2B.phosphorylated,< 2B,UNK
    >,<[2B - phos],UNK >,none)pnTrans('11rjr.Erks.are.phosphorylated,< Erks,UNK
    >,<[Erks - phos],UNK >,none)pnTrans('20rjr.GAP334.is.ubiquitinated,<
    GAP334,UNK >,<[GAP334 - ubiq],UNK >,none)pnTrans('10rjr.GTP.is.hydrolysed,<
    GTP,UNK >,< GDP,UNK >,none)pnTrans('21rjr.KRas15.is.ubiquitinated,< KRas15,
    UNK >,<[KRas15 - ubiq],UNK >,none)pnTrans('23rjr.Kras.is.ubiquitinated,<
    Kras,UNK >,<[Kras - ubiq],UNK >,none)pnTrans(
    '14rjr.MAPK2.is.phosphorylated,< MAPK2,UNK >,<[MAPK2 - phos],UNK >,
    none)pnTrans('16rjr.Rass.is.phosphorylated,< Rass,UNK >,<[Rass - phos],UNK
    >,none)pnTrans('25rjr.Rass.is.ubiquitinated,< Rass,UNK >,<[Rass - ubiq],UNK
    >,none)pnTrans('24rjr.Rass.is.ubiquitinated.on.K147,< Rass,UNK >,<[Rass -
    ubiq(K 147)],UNK >,none)pnTrans('15rjr.MAPK2.is.phosphorylated,< Tp53,UNK
    >,<[Tp53 - phos],UNK >,none)pnTrans('12rjr.Tp53bp1.is.phosphorylated,<
    Tp53bp1,UNK >,<[Tp53bp1 - phos],UNK >,none)pnTrans(
    '13rjr.Tp53bp2.is.phosphorylated,< Tp53bp2,CLc >,<[Tp53bp2 - phos],CLc >,
    none)pnTrans('13rjr.Tp53bp2.is.phosphorylated#1,< Tp53bp2,CLm >,<[Tp53bp2 -
    phos],CLm >,none)pnTrans('13rjr.Tp53bp2.is.phosphorylated#2,< Tp53bp2,UNK
    >,<[Tp53bp2 - phos],UNK >,none)pnTrans(
    '13rjr.Tp53bp2.is.phosphorylated.on.S827,< Tp53bp2,CLc >,<[Tp53bp2 - phos(S
    827)],CLc >,none)pnTrans('13rjr.Tp53bp2.is.phosphorylated.on.S827#1,<
    Tp53bp2,CLm >,<[Tp53bp2 - phos(S 827)],CLm >,none)pnTrans(
    '13rjr.Tp53bp2.is.phosphorylated.on.S827#2,< Tp53bp2,UNK >,<[Tp53bp2 -
    phos(S 827)],UNK >,none)pnTrans('18rjr.Tp53bp2.from.CLm.to.CLc,< Tp53bp2,
    CLm >,< Tp53bp2,CLc >,none)pnTrans('22rjr.Ubiquitin.is.ubiquitinated,<
    Ubiquitin,UNK >,<[Ubiquitin - ubiq],UNK >,none)pnTrans(
    '19rjr.mUbRas.is.ubiquitinated,< mUbRas,UNK >,<[mUbRas - ubiq],UNK >,
    none)pnTrans('8rjr.RasK147C.hydrolyses.GTP,<[RasK147C - GTP],UNK >,<[
    RasK147C - GDP],UNK >,none)pnTrans('9rjr.Rass.hydrolyses.GTP,<[Rass - GTP],
    UNK >,<[Rass - GDP],UNK >,none)pnTrans('7rjr.mUbRas.hydrolyses.GTP,<[mUbRas
    - GTP],UNK >,<[mUbRas - GDP],UNK >,none)pnTrans(
    '27rjr.Tp53bp2.phos.upreg.by.Erks,< Tp53bp2,UNK >,<[Tp53bp2 - phos],UNK >,<
    Erks,UNK >)pnTrans('26rjr.GTP.hydrolysis.upreg.by.Gaps,< GTP,UNK >,< GDP,
    UNK >,< Gaps,UNK >)pnTrans('6rjr.Rass.binds.GTP,< Rass,UNK > < GTP,UNK >,<[
    Rass - GTP],UNK >,none)pnTrans('3rjr.Tp53bp2.binds.Ras,< Rass,UNK > <
    Tp53bp2,UNK >,< Rass : Tp53bp2,UNK >,none)pnTrans(
    '17rjr.Tp53bp2.phos.is.regulated.by.Rass,< Tp53bp2,UNK >,<[Tp53bp2 - phos],
    UNK >,< Rass,UNK >)pnTrans('28rjr.Tp53bp2.phos.upreg.by.Rass,< Tp53bp2,UNK
    >,<[Tp53bp2 - phos],UNK >,< Rass,UNK >)pnTrans(
    '5rjr.Rass.binds.UbiquitinC77,< Rass,UNK > < UbiquitinC77,UNK >,< Rass :
    UbiquitinC77,UNK >,none)pnTrans('5rjr.Rass.binds.UbiquitinC76C,< Rass,UNK >
    < UbiquitinG76C,UNK >,< Rass : UbiquitinG76C,UNK >,none)pnTrans(
    '2rjr.Tp53bp1.binds.Tp53bp2,< Tp53bp1,UNK > < Tp53bp2,UNK >,< Tp53bp1 :
    Tp53bp2,UNK >,none)pnTrans('4rjr.GAP.binds.mUbRas,< Tp53bp2,UNK > < mUbRas,
    UNK >,< Tp53bp2 : mUbRas,UNK >,none)pnTrans(
    '1rjr.Tp53bp1.Tp53bp2.binds.Rass.GTP,< Tp53bp1,UNK > < Tp53bp2,UNK > <[Rass
    - GTP],UNK >,< Tp53bp1 : Tp53bp2 :[Rass - GTP],UNK >,none)))
