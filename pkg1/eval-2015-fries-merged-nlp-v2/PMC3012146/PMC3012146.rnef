<batch>
 <resnet mref="20871633:1099" msrc="Our finding that ID{9445010=genotoxic stress} promotes interaction between Rb and Hdm2 parallels recent findings that a ID{7157=p53} activating compound promotes ID{4193=Mdm2}-mediated degradation of p21, both ultimately leading to ID{4108219=cell death ( )}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:4193">
    <attr name="Name" value="MDM2" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:7157">
    <attr name="Name" value="TP53" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:100" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1060" msrc="In our experiments, ID{9445010=genotoxic stress} or activation of endogenous p38 with ID{5606=Ad-MKK3} resulted not only in hyperphosphorylation of Rb but also in a precipitous decline in total ID{5925=Rb protein} levels, suggesting that p38-mediated phosphorylation causes degradation of Rb ( ).">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:5925">
    <attr name="Name" value="RB1" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-meshdis:Genotoxic%20Stress">
    <attr name="Name" value="Genotoxic Stress" />
    <attr name="NodeType" value="Disease" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="StateChange" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:61" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="ChangeType" value="phosphorylation" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1026" msrc="To explore why these mechanisms for inactivating Rb that have been assumed to be functionally equivalent result in such strikingly different phenotypic outcomes, we developed an experimental model system in which Rb degradation and ID{4000007=apoptosis} were induced by ID{9445010=genotoxic stress} in Mel202 cells, which have been extensively characterized and shown to be wildtype for ID{7157=Rb and p53} ( ).">
  <nodes>
   <node local_id="N1" urn="urn:agi-gocellproc:0006915">
    <attr name="Name" value="apoptosis" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-meshdis:Genotoxic%20Stress">
    <attr name="Name" value="Genotoxic Stress" />
    <attr name="NodeType" value="Disease" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="FunctionalAssociation" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:27" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1004" msrc="The anti-proliferative and anti-ID{4000007=apoptotic}ID{4000538=functions} of Rb can be biochemically uncoupled and are both mediated largely through interactions with ID{12815008=E2F transcription factors (E2Fs)} ( ).">
  <nodes>
   <node local_id="N1" urn="urn:agi-gocellproc:0006915">
    <attr name="Name" value="apoptosis" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-aopfc:0000099">
    <attr name="Name" value="E2F" />
    <attr name="NodeType" value="FunctionalClass" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="unknown" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:5" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1051" msrc="We reasoned that ID{13000016=stress}-related phosphorylation of Rb is unique in its ability to release ID{12815008=E2Fs} that can then activate ID{4000007=apoptotic} genes, and that this could account for the ID{4000007=apoptosis} that occurs when p38 phosphorylates Rb.">
  <nodes>
   <node local_id="N1" urn="urn:agi-gocellproc:0006915">
    <attr name="Name" value="apoptosis" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-aopfc:0000099">
    <attr name="Name" value="E2F" />
    <attr name="NodeType" value="FunctionalClass" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:52" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1052" msrc="To ID{31400444=test} this, we activated endogenous p38 in cells by expressing ID{5606=MKK3}, and used co-ID{31210494=immunoprecipitation} experiments to evaluate the interaction between ID{1869=E2F1}, one of the ID{12815008=E2F family members} involved in ID{4000007=apoptosis}, and ectopically expressed Rb.">
  <nodes>
   <node local_id="N1" urn="urn:agi-gocellproc:0006915">
    <attr name="Name" value="apoptosis" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-aopfc:0000099">
    <attr name="Name" value="E2F" />
    <attr name="NodeType" value="FunctionalClass" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="unknown" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:53" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1052" msrc="To ID{31400444=test} this, we activated endogenous p38 in cells by expressing ID{5606=MKK3}, and used co-ID{31210494=immunoprecipitation} experiments to evaluate the interaction between ID{1869=E2F1}, one of the ID{12815008=E2F family members} involved in ID{4000007=apoptosis}, and ectopically expressed Rb.">
  <nodes>
   <node local_id="N1" urn="urn:agi-gocellproc:0006915">
    <attr name="Name" value="apoptosis" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:1869">
    <attr name="Name" value="E2F1" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="unknown" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:53" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1075" msrc="In this study, we provide evidence for a pro-ID{4000007=apoptotic} signaling pathway induced by ID{9445010=genotoxic stress}, in which p38 phosphorylates Rb in a manner that triggers Hdm2-mediated Rb degradation, and ID{1869=E2F1}-dependent ID{4108219=cell death ( )}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-gocellproc:0008219">
    <attr name="Name" value="cell death" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:1869">
    <attr name="Name" value="E2F1" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="unknown" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:76" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1099" msrc="Our finding that ID{9445010=genotoxic stress} promotes interaction between Rb and Hdm2 parallels recent findings that a ID{7157=p53} activating compound promotes ID{4193=Mdm2}-mediated degradation of p21, both ultimately leading to ID{4108219=cell death ( )}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-gocellproc:0008219">
    <attr name="Name" value="cell death" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:7157">
    <attr name="Name" value="TP53" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="unknown" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:100" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1114" msrc="Without such a mechanism, activation of ID{7157=p53} might indiscriminately trigger both cell cycle arrest and ID{4000007=apoptotic} programs, leading to a chaotic and inefficient ID{4000000,4106950=response to stress}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-gocellproc:0006915">
    <attr name="Name" value="apoptosis" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:7157">
    <attr name="Name" value="TP53" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="unknown" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:115" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1114" msrc="Without such a mechanism, activation of ID{7157=p53} might indiscriminately trigger both cell cycle arrest and ID{4000007=apoptotic} programs, leading to a chaotic and inefficient ID{4000000,4106950=response to stress}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-gocellproc:0006950">
    <attr name="Name" value="response to stress" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:7157">
    <attr name="Name" value="TP53" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="unknown" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:115" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1110" msrc="Disruption of the ID{7157=Hdm2-p53} interaction would activate ID{7157=p53}, and release Hdm2; however, in this setting, activated p38 would phosphorylate Rb on ID{32000000=Ser567//567}, allowing Hdm2 to interact with Rb triggering its degradation.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:7157">
    <attr name="Name" value="TP53" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:7157">
    <attr name="Name" value="TP53" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="negative" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:111" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1060" msrc="In our experiments, ID{9445010=genotoxic stress} or activation of endogenous p38 with ID{5606=Ad-MKK3} resulted not only in hyperphosphorylation of Rb but also in a precipitous decline in total ID{5925=Rb protein} levels, suggesting that p38-mediated phosphorylation causes degradation of Rb ( ).">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:5925">
    <attr name="Name" value="RB1" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:5606">
    <attr name="Name" value="MAP2K3" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="ExpressionControl" />
    <attr name="Effect" value="negative" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:61" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1060" msrc="In our experiments, ID{9445010=genotoxic stress} or activation of endogenous p38 with ID{5606=Ad-MKK3} resulted not only in hyperphosphorylation of Rb but also in a precipitous decline in total ID{5925=Rb protein} levels, suggesting that p38-mediated phosphorylation causes degradation of Rb ( ).">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:5925">
    <attr name="Name" value="RB1" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:5606">
    <attr name="Name" value="MAP2K3" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="ExpressionControl" />
    <attr name="Effect" value="unknown" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:61" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1056" msrc="Recognizing that ID{7157=p53} is activated by ID{1198943=etoposide} as part of the ID{4000000,4106974=DNA damage response} ( ), we wished to determine whether ID{7157=p53} was required for p38-mediated Rb phosphorylation and/or ID{4000007=apoptosis}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:7157">
    <attr name="Name" value="TP53" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-cas:33419-42-0">
    <attr name="Name" value="etoposide" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:57" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1036" msrc="Importantly, chemical inhibition of p38 substantially reduced ID{1198943=etoposide}-induced ID{4000007=apoptosis ( )}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-gocellproc:0006915">
    <attr name="Name" value="apoptosis" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-cas:33419-42-0">
    <attr name="Name" value="etoposide" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:37" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1057" msrc="Knock down of ID{7157=p53} by ID{31218226=RNAi} had no effect on ID{1198943=etoposide}-induced Rb hyperphosphorylation or degradation ( ), but it did inhibit ID{1198943=etoposide}-induced ID{4108219=cell death ( )}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-gocellproc:0008219">
    <attr name="Name" value="cell death" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-cas:33419-42-0">
    <attr name="Name" value="etoposide" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:58" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20871633:1130" msrc="When indicated, cells were treated with ID{1198943=etoposide} (Sigma Aldrich, St. Louis, MO, USA), p38 inhibitor (ID{1071076=SB 203580}, Millipore, Billerica, MA, USA), ID{1017=Cdk2} inhibitor (ID{1053853=Roscovitine}, Sigma Aldrich), ID{1019=Cdk4} inhibitor (EMD, Darmstadt, Germany), Hdm2 E3 Ligase Inhibitor 373225 (EMD), Z-VAD-FMK (Sigma Aldrich) or ID{1272940=MG132} (Sigma Aldrich).">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:1017">
    <attr name="Name" value="CDK2" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-cas:186692-46-6">
    <attr name="Name" value="roscovitine" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="DirectRegulation" />
    <attr name="Effect" value="negative" />
    <attr name="Mechanism" value="direct interaction" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1476-5594" />
    <attr name="PubDay" value="1" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1038/onc.2010.442" />
    <attr name="TextRef" value="info:pmid/20871633#body:131" />
    <attr name="Journal" value="Oncogene" />
    <attr name="Pages" value="588-599" />
    <attr name="MedlineTA" value="Oncogene" />
    <attr name="PMC" value="3012146" />
    <attr name="Volume" value="30" />
    <attr name="Title" value="p38 phosphorylates Rb on Ser567 by a novel, cell cycle-independent mechanism that triggers Rb-Hdm2 interaction and apoptosis" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="20871633" />
    <attr name="Authors" value="Delston,R.B.;Matatall,K.A.;Sun,Y.;Onken,M.D.;Harbour,J.W." />
    <attr name="ISSN" value="0950-9232" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
</batch>
