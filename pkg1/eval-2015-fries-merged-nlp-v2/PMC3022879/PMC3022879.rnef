<batch>
 <resnet mref="21267416:1179" msrc="In summary, our data define a role for rapid fluctuations in ID{1800007=glucocorticoid} concentration on target gene regulation, and provides a possible biological impact for the physiological fluctuations in serum ID{1800007=glucocorticoid} concentrations.">
  <nodes>
   <node local_id="N1" urn="urn:agi-aogroup:0000723">
    <attr name="NodeType" value="Group" />
    <attr name="Name" value="Biofluids assay-able substances" />
   </node>
   <node local_id="N2" urn="urn:agi-smol:glucocorticoids">
    <attr name="Name" value="glucocorticoid" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="MemberOf" />
    <attr name="PubMonth" value="Jan" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="18" />
    <attr name="Issue" value="1" />
    <attr name="DOI" value="10.1371/journal.pone.0015766" />
    <attr name="TextRef" value="info:pmid/21267416#body:180" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="3022879" />
    <attr name="Volume" value="6" />
    <attr name="Title" value="Ultradian Cortisol Pulsatility Encodes a Distinct, Biologically Important Signal" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="21267416" />
    <attr name="Authors" value="McMaster,A.;Jangani,M.;Sommer,P.;Han,N.;Brass,A.;Beesley,S.;Lu,W.;Berry,A.;Loudon,A.;Donn,R.;Ray,D.W." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="21267416:1116" msrc="The expression of ID{8515=ITGA10} was increased more by continuous than pulsatile ID{1100010=cortisol}, and the expression of ID{976=CD97} was down-regulated, by all cortisol treatments ( ).">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:8515">
    <attr name="Name" value="ITGA10" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-cas:50-23-7">
    <attr name="Name" value="cortisol" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="ExpressionControl" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="Jan" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="18" />
    <attr name="Issue" value="1" />
    <attr name="DOI" value="10.1371/journal.pone.0015766" />
    <attr name="TextRef" value="info:pmid/21267416#body:117" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="3022879" />
    <attr name="Volume" value="6" />
    <attr name="Title" value="Ultradian Cortisol Pulsatility Encodes a Distinct, Biologically Important Signal" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="21267416" />
    <attr name="Authors" value="McMaster,A.;Jangani,M.;Sommer,P.;Han,N.;Brass,A.;Beesley,S.;Lu,W.;Berry,A.;Loudon,A.;Donn,R.;Ray,D.W." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="21267416:1127" msrc="ID{4128=MAOA} was predicted by the ID{31206047=microarray} to be induced to a greater extent by continuous ID{1100010=cortisol}, and this was seen by ID{31400865=qRT-PCR}, though it did not reach statistical significance, and ID{2308=FOXO1} was induced to a similar extent by both pulsatile, and continuous ID{1100010=cortisol} in both the ID{31206047=array}, and the ID{31400865=qRT-PCR}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:4128">
    <attr name="Name" value="MAOA" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-cas:50-23-7">
    <attr name="Name" value="cortisol" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="Jan" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="18" />
    <attr name="Issue" value="1" />
    <attr name="DOI" value="10.1371/journal.pone.0015766" />
    <attr name="TextRef" value="info:pmid/21267416#body:128" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="3022879" />
    <attr name="Volume" value="6" />
    <attr name="Title" value="Ultradian Cortisol Pulsatility Encodes a Distinct, Biologically Important Signal" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="21267416" />
    <attr name="Authors" value="McMaster,A.;Jangani,M.;Sommer,P.;Han,N.;Brass,A.;Beesley,S.;Lu,W.;Berry,A.;Loudon,A.;Donn,R.;Ray,D.W." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="21267416:1127" msrc="ID{4128=MAOA} was predicted by the ID{31206047=microarray} to be induced to a greater extent by continuous ID{1100010=cortisol}, and this was seen by ID{31400865=qRT-PCR}, though it did not reach statistical significance, and ID{2308=FOXO1} was induced to a similar extent by both pulsatile, and continuous ID{1100010=cortisol} in both the ID{31206047=array}, and the ID{31400865=qRT-PCR}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:2308">
    <attr name="Name" value="FOXO1" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-cas:50-23-7">
    <attr name="Name" value="cortisol" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="Jan" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="18" />
    <attr name="Issue" value="1" />
    <attr name="DOI" value="10.1371/journal.pone.0015766" />
    <attr name="TextRef" value="info:pmid/21267416#body:128" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="3022879" />
    <attr name="Volume" value="6" />
    <attr name="Title" value="Ultradian Cortisol Pulsatility Encodes a Distinct, Biologically Important Signal" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="21267416" />
    <attr name="Authors" value="McMaster,A.;Jangani,M.;Sommer,P.;Han,N.;Brass,A.;Beesley,S.;Lu,W.;Berry,A.;Loudon,A.;Donn,R.;Ray,D.W." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="21267416:1127" msrc="ID{4128=MAOA} was predicted by the ID{31206047=microarray} to be induced to a greater extent by continuous ID{1100010=cortisol}, and this was seen by ID{31400865=qRT-PCR}, though it did not reach statistical significance, and ID{2308=FOXO1} was induced to a similar extent by both pulsatile, and continuous ID{1100010=cortisol} in both the ID{31206047=array}, and the ID{31400865=qRT-PCR}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:2308">
    <attr name="Name" value="FOXO1" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-cas:50-23-7">
    <attr name="Name" value="cortisol" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="Jan" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="18" />
    <attr name="Issue" value="1" />
    <attr name="DOI" value="10.1371/journal.pone.0015766" />
    <attr name="TextRef" value="info:pmid/21267416#body:128" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="3022879" />
    <attr name="Volume" value="6" />
    <attr name="Title" value="Ultradian Cortisol Pulsatility Encodes a Distinct, Biologically Important Signal" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="21267416" />
    <attr name="Authors" value="McMaster,A.;Jangani,M.;Sommer,P.;Han,N.;Brass,A.;Beesley,S.;Lu,W.;Berry,A.;Loudon,A.;Donn,R.;Ray,D.W." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="21267416:1128" msrc="ID{6885=MAP3K7} was predicted by the ID{31206047=microarray} to be repressed to a greater extent by pulsatile ID{1100010=cortisol}, and this effect was not confirmed by ID{31400865=qRT-PCR} measurement.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:6885">
    <attr name="Name" value="MAP3K7" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-cas:50-23-7">
    <attr name="Name" value="cortisol" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="negative" />
    <attr name="PubMonth" value="Jan" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="18" />
    <attr name="Issue" value="1" />
    <attr name="DOI" value="10.1371/journal.pone.0015766" />
    <attr name="TextRef" value="info:pmid/21267416#body:129" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="3022879" />
    <attr name="Volume" value="6" />
    <attr name="Title" value="Ultradian Cortisol Pulsatility Encodes a Distinct, Biologically Important Signal" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="21267416" />
    <attr name="Authors" value="McMaster,A.;Jangani,M.;Sommer,P.;Han,N.;Brass,A.;Beesley,S.;Lu,W.;Berry,A.;Loudon,A.;Donn,R.;Ray,D.W." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="21267416:1087" msrc="Pulsatile ID{1100010=cortisol} caused a significant reduction in live ID{4003409=cell survival} compared to continuous exposure, reflecting a complex cell response to ID{1100010=cortisol} delivery kinetics ( ). ID{1800007=(A) Glucocorticoid} regulation of ID{4108283=cell proliferation}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-cellproc:cell%20survival">
    <attr name="Name" value="cell survival" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-cas:50-23-7">
    <attr name="Name" value="cortisol" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="negative" />
    <attr name="PubMonth" value="Jan" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="18" />
    <attr name="Issue" value="1" />
    <attr name="DOI" value="10.1371/journal.pone.0015766" />
    <attr name="TextRef" value="info:pmid/21267416#body:88" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="3022879" />
    <attr name="Volume" value="6" />
    <attr name="Title" value="Ultradian Cortisol Pulsatility Encodes a Distinct, Biologically Important Signal" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="21267416" />
    <attr name="Authors" value="McMaster,A.;Jangani,M.;Sommer,P.;Han,N.;Brass,A.;Beesley,S.;Lu,W.;Berry,A.;Loudon,A.;Donn,R.;Ray,D.W." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="21267416:1114" msrc="The cluster heat map of ID{4107155=cell adhesion} genes affected by the mode of ID{1100010=cortisol} delivery is shown in .">
  <nodes>
   <node local_id="N1" urn="urn:agi-gocellproc:0007155">
    <attr name="Name" value="cell adhesion" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-cas:50-23-7">
    <attr name="Name" value="cortisol" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="unknown" />
    <attr name="PubMonth" value="Jan" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="18" />
    <attr name="Issue" value="1" />
    <attr name="DOI" value="10.1371/journal.pone.0015766" />
    <attr name="TextRef" value="info:pmid/21267416#body:115" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="3022879" />
    <attr name="Volume" value="6" />
    <attr name="Title" value="Ultradian Cortisol Pulsatility Encodes a Distinct, Biologically Important Signal" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="21267416" />
    <attr name="Authors" value="McMaster,A.;Jangani,M.;Sommer,P.;Han,N.;Brass,A.;Beesley,S.;Lu,W.;Berry,A.;Loudon,A.;Donn,R.;Ray,D.W." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="21267416:1132" msrc="Importantly, pulsatile and continuous ID{1100010=cortisol} exerted quite different effects on ID{9001339=mammary tumor} virus promoter activity, as predicted ( ). (A) The significantly different transcription factor activities in the continuous ID{1100010=cortisol} treatment (100 ng/ml) (CCT) compared with the pulsatile ID{1100010=cortisol} treatment (of matched dose) are presented in the limpet-like plots.">
  <nodes>
   <node local_id="N1" urn="urn:agi-meshdis:Breast%20Neoplasms">
    <attr name="Name" value="Breast Neoplasms" />
    <attr name="NodeType" value="Disease" />
   </node>
   <node local_id="N2" urn="urn:agi-cas:50-23-7">
    <attr name="Name" value="cortisol" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="negative" />
    <attr name="PubMonth" value="Jan" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="18" />
    <attr name="Issue" value="1" />
    <attr name="DOI" value="10.1371/journal.pone.0015766" />
    <attr name="TextRef" value="info:pmid/21267416#body:133" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="3022879" />
    <attr name="Volume" value="6" />
    <attr name="TextMods" value="82: 'MMTV ' -&gt; 'mammary tumor virus ' 171: '(SigDiff) ' -&gt; '' 203: '(TFAs) ' -&gt; '' 305: '(PCT) ' -&gt; ''" />
    <attr name="Title" value="Ultradian Cortisol Pulsatility Encodes a Distinct, Biologically Important Signal" />
    <attr name="PubYear" value="2011" />
    <attr name="PMID" value="21267416" />
    <attr name="Authors" value="McMaster,A.;Jangani,M.;Sommer,P.;Han,N.;Brass,A.;Beesley,S.;Lu,W.;Berry,A.;Loudon,A.;Donn,R.;Ray,D.W." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
</batch>
