<batch>
 <resnet mref="24809459:1000" msrc="ID{9004292=Mycobacterium tuberculosis} is an extraordinarily versatile ID{13003353=pathogen} that can exist in two distinct states in the host, leading to asymptomatic ID{9603830=latent infection} in which bacilli are present in a non-replicating dormant form, or to active ID{9004292=tuberculosis} , characterized by actively replicating organisms.">
  <nodes>
   <node local_id="N1" urn="urn:agi-disease:latent%20infection">
    <attr name="Name" value="latent infection" />
    <attr name="NodeType" value="Disease" />
   </node>
   <node local_id="N2" urn="urn:agi-meshdis:Tuberculosis">
    <attr name="Name" value="Tuberculosis" />
    <attr name="NodeType" value="Disease" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="FunctionalAssociation" />
    <attr name="PubMonth" value="May" />
    <attr name="ESSN" value="1553-7374" />
    <attr name="PubDay" value="8" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1371/journal.ppat.1004115" />
    <attr name="TextRef" value="info:pmid/24809459#body:1" />
    <attr name="Journal" value="PLoS Pathogens" />
    <attr name="MedlineTA" value="PLoS Pathog" />
    <attr name="PMC" value="4014462" />
    <attr name="Volume" value="10" />
    <attr name="TextMods" value="27: '(Mtb) ' -&gt; '' 250: '(TB)' -&gt; ''" />
    <attr name="Title" value="Phosphorylation of KasB Regulates Virulence and Acid-Fastness in Mycobacterium tuberculosis" />
    <attr name="PubYear" value="2014" />
    <attr name="PMID" value="24809459" />
    <attr name="Authors" value="Vilchèze,C.;Molle,V.;Carrère-Kremer,S.;Leiba,J.;Mourey,L.;Shenai,S.;Baronian,G.;Tufariello,J.;Hartman,T.;Veyron-Churlet,R.;Trivelli,X.;Tiwari,S.;Weinrick,B.;Alland,D.;Guérardel,Y.;Jacobs,W.R.;Kremer,L." />
    <attr name="ISSN" value="1553-7366" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="24809459:1247" msrc="Earlier studies demonstrated that polyacylated ID{1109010=trehalose} deficiency affects the surface global composition of the mycobacterial cell envelope, improving the efficiency with which ID{9004292=Mycobacterium tuberculosis} binds to and enters phagocytic host cells , implying that polyacylated ID{1109010=trehalose} production affects early interaction between ID{9004292=Mycobacterium tuberculosis} and ID{10000094=macrophages}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-meshdis:Tuberculosis">
    <attr name="Name" value="Tuberculosis" />
    <attr name="NodeType" value="Disease" />
   </node>
   <node local_id="N2" urn="urn:agi-cas:99-20-7">
    <attr name="Name" value="trehalose" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="negative" />
    <attr name="PubMonth" value="May" />
    <attr name="ESSN" value="1553-7374" />
    <attr name="PubDay" value="8" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1371/journal.ppat.1004115" />
    <attr name="TextRef" value="info:pmid/24809459#body:248" />
    <attr name="Journal" value="PLoS Pathogens" />
    <attr name="MedlineTA" value="PLoS Pathog" />
    <attr name="PMC" value="4014462" />
    <attr name="Volume" value="10" />
    <attr name="TextMods" value="34: 'PAT ' -&gt; 'polyacylated trehalose ' 179: 'Mtb ' -&gt; 'Mycobacterium tuberculosis ' 264: 'PAT ' -&gt; 'polyacylated trehalose ' 332: 'Mtb ' -&gt; 'Mycobacterium tuberculosis '" />
    <attr name="Title" value="Phosphorylation of KasB Regulates Virulence and Acid-Fastness in Mycobacterium tuberculosis" />
    <attr name="PubYear" value="2014" />
    <attr name="PMID" value="24809459" />
    <attr name="Authors" value="Vilchèze,C.;Molle,V.;Carrère-Kremer,S.;Leiba,J.;Mourey,L.;Shenai,S.;Baronian,G.;Tufariello,J.;Hartman,T.;Veyron-Churlet,R.;Trivelli,X.;Tiwari,S.;Weinrick,B.;Alland,D.;Guérardel,Y.;Jacobs,W.R.;Kremer,L." />
    <attr name="ISSN" value="1553-7366" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="24809459:1219" msrc="This leads to the production of shorter ID{1419679=mycolic acids} which is associated to dramatic phenotype changes, such as loss of acid-fastness, decreased ID{15213121=cell wall permeability}, severe attenuation in infected ID{7000389=mice} and defect in ID{10000094=macrophage} colonization.">
  <nodes>
   <node local_id="N1" urn="urn:agi-clinpar:cell%20membrane%20permeability">
    <attr name="Name" value="cell membrane permeability" />
    <attr name="NodeType" value="ClinicalParameter" />
   </node>
   <node local_id="N2" urn="urn:agi-cas:37281-34-8">
    <attr name="Name" value="mycolic acid" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="May" />
    <attr name="ESSN" value="1553-7374" />
    <attr name="PubDay" value="8" />
    <attr name="Issue" value="5" />
    <attr name="DOI" value="10.1371/journal.ppat.1004115" />
    <attr name="TextRef" value="info:pmid/24809459#body:220" />
    <attr name="Journal" value="PLoS Pathogens" />
    <attr name="MedlineTA" value="PLoS Pathog" />
    <attr name="PMC" value="4014462" />
    <attr name="Volume" value="10" />
    <attr name="Title" value="Phosphorylation of KasB Regulates Virulence and Acid-Fastness in Mycobacterium tuberculosis" />
    <attr name="PubYear" value="2014" />
    <attr name="PMID" value="24809459" />
    <attr name="Authors" value="Vilchèze,C.;Molle,V.;Carrère-Kremer,S.;Leiba,J.;Mourey,L.;Shenai,S.;Baronian,G.;Tufariello,J.;Hartman,T.;Veyron-Churlet,R.;Trivelli,X.;Tiwari,S.;Weinrick,B.;Alland,D.;Guérardel,Y.;Jacobs,W.R.;Kremer,L." />
    <attr name="ISSN" value="1553-7366" />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
</batch>
