<batch>
 <resnet mref="12184796:1010" msrc="These results suggest that the PDZ domains of ID{1742=PSD-95} may facilitate the assembly of signaling complexes involving both ID{0,4842,3117146=NMDA receptors and nNOS}, and the increases in intracellular ID{1093823=Ca2+} caused by ID{3117146=NMDA receptor} activation may facilitate ID{4842=nNOS} activation.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:4842">
    <attr name="Name" value="NOS1" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-gocomplex:0017146">
    <attr name="Name" value="N-methyl-D-aspartate selective glutamate receptor" />
    <attr name="NodeType" value="Complex" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1471-2210" />
    <attr name="PubDay" value="16" />
    <attr name="DOI" value="10.1186/1471-2210-2-17" />
    <attr name="TextRef" value="info:pmid/12184796#body:11" />
    <attr name="Journal" value="BMC Pharmacology" />
    <attr name="Pages" value="17-17" />
    <attr name="MedlineTA" value="BMC Pharmacol" />
    <attr name="PMC" value="128815" />
    <attr name="Volume" value="2" />
    <attr name="Title" value="Interaction of neuronal nitric oxide synthase with alpha1" />
    <attr name="PubYear" value="2002" />
    <attr name="PMID" value="12184796" />
    <attr name="Authors" value="Pupo,A.S.;Minneman,K.P." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="12184796:1009" msrc="Consequently, ID{3117146=NMDA receptor} activation increases ID{1098945=nitric oxide} production and ID{9017831=neurotoxicity}; while suppression of ID{1742=PSD-95} expression inhibits these responses.">
  <nodes>
   <node local_id="N1" urn="urn:agi-disease:neuron%20toxicity">
    <attr name="Name" value="neuron toxicity" />
    <attr name="NodeType" value="Disease" />
   </node>
   <node local_id="N2" urn="urn:agi-gocomplex:0017146">
    <attr name="Name" value="N-methyl-D-aspartate selective glutamate receptor" />
    <attr name="NodeType" value="Complex" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1471-2210" />
    <attr name="PubDay" value="16" />
    <attr name="DOI" value="10.1186/1471-2210-2-17" />
    <attr name="TextRef" value="info:pmid/12184796#body:10" />
    <attr name="Journal" value="BMC Pharmacology" />
    <attr name="Pages" value="17-17" />
    <attr name="MedlineTA" value="BMC Pharmacol" />
    <attr name="PMC" value="128815" />
    <attr name="Volume" value="2" />
    <attr name="Title" value="Interaction of neuronal nitric oxide synthase with alpha1" />
    <attr name="PubYear" value="2002" />
    <attr name="PMID" value="12184796" />
    <attr name="Authors" value="Pupo,A.S.;Minneman,K.P." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="12184796:1068" msrc="It is known that ID{1098945=nitric oxide} produced by ID{4842=nNOS} is required for PC12 ID{4130154=cell differentiation} induced by ID{4803=nerve growth factor} and that treatment of PC12 cells with ID{4803=nerve growth factor} induces ID{4842=nNOS} expression[ ].">
  <nodes>
   <node local_id="N1" urn="urn:agi-gocellproc:0030154">
    <attr name="Name" value="cell differentiation" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:4803">
    <attr name="Name" value="NGF" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1471-2210" />
    <attr name="PubDay" value="16" />
    <attr name="DOI" value="10.1186/1471-2210-2-17" />
    <attr name="TextRef" value="info:pmid/12184796#body:69" />
    <attr name="Journal" value="BMC Pharmacology" />
    <attr name="Pages" value="17-17" />
    <attr name="MedlineTA" value="BMC Pharmacol" />
    <attr name="PMC" value="128815" />
    <attr name="Volume" value="2" />
    <attr name="TextMods" value="120: '(NGF) ' -&gt; '' 158: 'NGF ' -&gt; 'nerve growth factor '" />
    <attr name="Title" value="Interaction of neuronal nitric oxide synthase with alpha1" />
    <attr name="PubYear" value="2002" />
    <attr name="PMID" value="12184796" />
    <attr name="Authors" value="Pupo,A.S.;Minneman,K.P." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="12184796:1069" msrc="Since ID{12104937=α1A-α1-Adrenergic receptor} stimulation also activates ERKS and induces ID{4130154=differentiation} in PC12 cells stably transfected with this subtype[ ], we investigated the effects of l-NAME, an inhibitor of NOS, on ID{1267744=norepinephrine}-induced ID{12815114=ERK} phosphorylation in ID{11011888=PC12 cells} stably transfected with HFID{148=α1A-ARs}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-gocellproc:0030154">
    <attr name="Name" value="cell differentiation" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-go:0004937">
    <attr name="Name" value="alpha1 adrenoceptor" />
    <attr name="NodeType" value="FunctionalClass" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1471-2210" />
    <attr name="PubDay" value="16" />
    <attr name="DOI" value="10.1186/1471-2210-2-17" />
    <attr name="TextRef" value="info:pmid/12184796#body:70" />
    <attr name="Journal" value="BMC Pharmacology" />
    <attr name="Pages" value="17-17" />
    <attr name="MedlineTA" value="BMC Pharmacol" />
    <attr name="PMC" value="128815" />
    <attr name="Volume" value="2" />
    <attr name="TextMods" value="11: 'AR ' -&gt; 'α1-Adrenergic receptor '" />
    <attr name="Title" value="Interaction of neuronal nitric oxide synthase with alpha1" />
    <attr name="PubYear" value="2002" />
    <attr name="PMID" value="12184796" />
    <attr name="Authors" value="Pupo,A.S.;Minneman,K.P." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="12184796:1008" msrc="For example, ID{3117146=NMDA receptors} in ID{31200874=cultured cortical neurons} associate with ID{4842=nNOS} through ID{1742=PSD-95}, a protein containing three PDZ domains[ ].">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:1742">
    <attr name="Name" value="DLG4" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-gocomplex:0017146">
    <attr name="Name" value="N-methyl-D-aspartate selective glutamate receptor" />
    <attr name="NodeType" value="Complex" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="Binding" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1471-2210" />
    <attr name="PubDay" value="16" />
    <attr name="DOI" value="10.1186/1471-2210-2-17" />
    <attr name="TextRef" value="info:pmid/12184796#body:9" />
    <attr name="Journal" value="BMC Pharmacology" />
    <attr name="Pages" value="17-17" />
    <attr name="MedlineTA" value="BMC Pharmacol" />
    <attr name="PMC" value="128815" />
    <attr name="Volume" value="2" />
    <attr name="Title" value="Interaction of neuronal nitric oxide synthase with alpha1" />
    <attr name="PubYear" value="2002" />
    <attr name="PMID" value="12184796" />
    <attr name="Authors" value="Pupo,A.S.;Minneman,K.P." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="12184796:1014" msrc="We found that ID{4842=nNOS} does interact with full-length ID{148=α1A-ARs}, but that it also interacts with other ID{12104937=α1-α1-Adrenergic receptor} subtypes and ID{12104939=β-ARs}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:148">
    <attr name="Name" value="ADRA1A" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:4842">
    <attr name="Name" value="NOS1" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="Binding" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1471-2210" />
    <attr name="PubDay" value="16" />
    <attr name="DOI" value="10.1186/1471-2210-2-17" />
    <attr name="TextRef" value="info:pmid/12184796#body:15" />
    <attr name="Journal" value="BMC Pharmacology" />
    <attr name="Pages" value="17-17" />
    <attr name="MedlineTA" value="BMC Pharmacol" />
    <attr name="PMC" value="128815" />
    <attr name="Volume" value="2" />
    <attr name="TextMods" value="102: 'AR ' -&gt; 'α1-Adrenergic receptor '" />
    <attr name="Title" value="Interaction of neuronal nitric oxide synthase with alpha1" />
    <attr name="PubYear" value="2002" />
    <attr name="PMID" value="12184796" />
    <attr name="Authors" value="Pupo,A.S.;Minneman,K.P." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="12184796:1078" msrc="The domain of the ID{624=bradykinin B2 receptor} that interacts with ID{4842=nNOS} is in the C-tail shortly after the predicted 7th transmembrane domain, and spatially similar but structurally dissimilar domains of the ID{7000383=rat} ID{185=angiotensin AT1 receptor} and ID{1183571=human endothelin-1} ID{1910=ETB receptors} have been proposed to block ID{4846=endothelial NOS (eNOS)} activity, possibly through a similar mechanism[ ].">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:4842">
    <attr name="Name" value="NOS1" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:624">
    <attr name="Name" value="BDKRB2" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="Binding" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1471-2210" />
    <attr name="PubDay" value="16" />
    <attr name="DOI" value="10.1186/1471-2210-2-17" />
    <attr name="TextRef" value="info:pmid/12184796#body:79" />
    <attr name="Journal" value="BMC Pharmacology" />
    <attr name="Pages" value="17-17" />
    <attr name="MedlineTA" value="BMC Pharmacol" />
    <attr name="PMC" value="128815" />
    <attr name="Volume" value="2" />
    <attr name="Title" value="Interaction of neuronal nitric oxide synthase with alpha1" />
    <attr name="PubYear" value="2002" />
    <attr name="PMID" value="12184796" />
    <attr name="Authors" value="Pupo,A.S.;Minneman,K.P." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="12184796:1078" msrc="The domain of the ID{624=bradykinin B2 receptor} that interacts with ID{4842=nNOS} is in the C-tail shortly after the predicted 7th transmembrane domain, and spatially similar but structurally dissimilar domains of the ID{7000383=rat} ID{185=angiotensin AT1 receptor} and ID{1183571=human endothelin-1} ID{1910=ETB receptors} have been proposed to block ID{4846=endothelial NOS (eNOS)} activity, possibly through a similar mechanism[ ].">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:4846">
    <attr name="Name" value="NOS3" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:1910">
    <attr name="Name" value="EDNRB" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="negative" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1471-2210" />
    <attr name="PubDay" value="16" />
    <attr name="DOI" value="10.1186/1471-2210-2-17" />
    <attr name="TextRef" value="info:pmid/12184796#body:79" />
    <attr name="Journal" value="BMC Pharmacology" />
    <attr name="Pages" value="17-17" />
    <attr name="MedlineTA" value="BMC Pharmacol" />
    <attr name="PMC" value="128815" />
    <attr name="Volume" value="2" />
    <attr name="Title" value="Interaction of neuronal nitric oxide synthase with alpha1" />
    <attr name="PubYear" value="2002" />
    <attr name="PMID" value="12184796" />
    <attr name="Authors" value="Pupo,A.S.;Minneman,K.P." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="12184796:1080" msrc="Co-ID{31210494=immunoprecipitation} experiments showed that epitope-tagged ID{148=α1A-ARs} do interact with full-length ID{4842=nNOS} when expressed together in ID{11027204=HEK-293 cells}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:4842">
    <attr name="Name" value="NOS1" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:148">
    <attr name="Name" value="ADRA1A" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="Binding" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1471-2210" />
    <attr name="PubDay" value="16" />
    <attr name="DOI" value="10.1186/1471-2210-2-17" />
    <attr name="TextRef" value="info:pmid/12184796#body:81" />
    <attr name="Journal" value="BMC Pharmacology" />
    <attr name="Pages" value="17-17" />
    <attr name="MedlineTA" value="BMC Pharmacol" />
    <attr name="PMC" value="128815" />
    <attr name="Volume" value="2" />
    <attr name="Title" value="Interaction of neuronal nitric oxide synthase with alpha1" />
    <attr name="PubYear" value="2002" />
    <attr name="PMID" value="12184796" />
    <attr name="Authors" value="Pupo,A.S.;Minneman,K.P." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="12184796:1097" msrc="Our data suggest that full-length ID{148=α1A-ARs} do interact with ID{4842=nNOS}; however this interaction is not subtype-specific since α1B- and ID{146=α1D-ARs} showed similar interactions.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:4842">
    <attr name="Name" value="NOS1" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:148">
    <attr name="Name" value="ADRA1A" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="Binding" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1471-2210" />
    <attr name="PubDay" value="16" />
    <attr name="DOI" value="10.1186/1471-2210-2-17" />
    <attr name="TextRef" value="info:pmid/12184796#body:98" />
    <attr name="Journal" value="BMC Pharmacology" />
    <attr name="Pages" value="17-17" />
    <attr name="MedlineTA" value="BMC Pharmacol" />
    <attr name="PMC" value="128815" />
    <attr name="Volume" value="2" />
    <attr name="Title" value="Interaction of neuronal nitric oxide synthase with alpha1" />
    <attr name="PubYear" value="2002" />
    <attr name="PMID" value="12184796" />
    <attr name="Authors" value="Pupo,A.S.;Minneman,K.P." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="12184796:1009" msrc="Consequently, ID{3117146=NMDA receptor} activation increases ID{1098945=nitric oxide} production and ID{9017831=neurotoxicity}; while suppression of ID{1742=PSD-95} expression inhibits these responses.">
  <nodes>
   <node local_id="N1" urn="urn:agi-cas:10102-43-9">
    <attr name="Name" value="NO" />
    <attr name="NodeType" value="SmallMol" />
   </node>
   <node local_id="N2" urn="urn:agi-gocomplex:0017146">
    <attr name="Name" value="N-methyl-D-aspartate selective glutamate receptor" />
    <attr name="NodeType" value="Complex" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="MolSynthesis" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1471-2210" />
    <attr name="PubDay" value="16" />
    <attr name="DOI" value="10.1186/1471-2210-2-17" />
    <attr name="TextRef" value="info:pmid/12184796#body:10" />
    <attr name="Journal" value="BMC Pharmacology" />
    <attr name="Pages" value="17-17" />
    <attr name="MedlineTA" value="BMC Pharmacol" />
    <attr name="PMC" value="128815" />
    <attr name="Volume" value="2" />
    <attr name="Title" value="Interaction of neuronal nitric oxide synthase with alpha1" />
    <attr name="PubYear" value="2002" />
    <attr name="PMID" value="12184796" />
    <attr name="Authors" value="Pupo,A.S.;Minneman,K.P." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="12184796:1010" msrc="These results suggest that the PDZ domains of ID{1742=PSD-95} may facilitate the assembly of signaling complexes involving both ID{0,4842,3117146=NMDA receptors and nNOS}, and the increases in intracellular ID{1093823=Ca2+} caused by ID{3117146=NMDA receptor} activation may facilitate ID{4842=nNOS} activation.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:4842">
    <attr name="Name" value="NOS1" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-cas:14127-61-8">
    <attr name="Name" value="Ca2+" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1471-2210" />
    <attr name="PubDay" value="16" />
    <attr name="DOI" value="10.1186/1471-2210-2-17" />
    <attr name="TextRef" value="info:pmid/12184796#body:11" />
    <attr name="Journal" value="BMC Pharmacology" />
    <attr name="Pages" value="17-17" />
    <attr name="MedlineTA" value="BMC Pharmacol" />
    <attr name="PMC" value="128815" />
    <attr name="Volume" value="2" />
    <attr name="Title" value="Interaction of neuronal nitric oxide synthase with alpha1" />
    <attr name="PubYear" value="2002" />
    <attr name="PMID" value="12184796" />
    <attr name="Authors" value="Pupo,A.S.;Minneman,K.P." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="12184796:1068" msrc="It is known that ID{1098945=nitric oxide} produced by ID{4842=nNOS} is required for PC12 ID{4130154=cell differentiation} induced by ID{4803=nerve growth factor} and that treatment of PC12 cells with ID{4803=nerve growth factor} induces ID{4842=nNOS} expression[ ].">
  <nodes>
   <node local_id="N1" urn="urn:agi-gocellproc:0030154">
    <attr name="Name" value="cell differentiation" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-cas:10102-43-9">
    <attr name="Name" value="NO" />
    <attr name="NodeType" value="SmallMol" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="unknown" />
    <attr name="PubMonth" value="Aug" />
    <attr name="ESSN" value="1471-2210" />
    <attr name="PubDay" value="16" />
    <attr name="DOI" value="10.1186/1471-2210-2-17" />
    <attr name="TextRef" value="info:pmid/12184796#body:69" />
    <attr name="Journal" value="BMC Pharmacology" />
    <attr name="Pages" value="17-17" />
    <attr name="MedlineTA" value="BMC Pharmacol" />
    <attr name="PMC" value="128815" />
    <attr name="Volume" value="2" />
    <attr name="TextMods" value="120: '(NGF) ' -&gt; '' 158: 'NGF ' -&gt; 'nerve growth factor '" />
    <attr name="Title" value="Interaction of neuronal nitric oxide synthase with alpha1" />
    <attr name="PubYear" value="2002" />
    <attr name="PMID" value="12184796" />
    <attr name="Authors" value="Pupo,A.S.;Minneman,K.P." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
</batch>
