<batch>
 <resnet mref="20634958:1004" msrc="Among them, ID{2725=optineurin} is a gene that links particularly to ID{9104460=normal pressure glaucoma}, a subtype of ID{9002995=primary open angle glaucoma} that accounts for approximately 30% of the ID{9002995=primary open angle glaucoma} cases .">
  <nodes>
   <node local_id="N1" urn="urn:agi-meshdis:Glaucoma%2c%20Open-Angle">
    <attr name="Name" value="Glaucoma, Open-Angle" />
    <attr name="NodeType" value="Disease" />
   </node>
   <node local_id="N2" urn="urn:agi-meshdis:Low%20Tension%20Glaucoma">
    <attr name="Name" value="Low Tension Glaucoma" />
    <attr name="NodeType" value="Disease" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="FunctionalAssociation" />
    <attr name="PubMonth" value="Jul" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="12" />
    <attr name="Issue" value="7" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="TextRef" value="info:pmid/20634958#body:5" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="2902519" />
    <attr name="Volume" value="5" />
    <attr name="TextMods" value="99: 'POAG ' -&gt; 'primary open angle glaucoma ' 170: 'POAG ' -&gt; 'primary open angle glaucoma '" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PubYear" value="2010" />
    <attr name="PMID" value="20634958" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1254" msrc="Based on our current and previous findings, we surmise that the defective trafficking, along with fragmentation of the ID{2000000,2000916=Golgi} complex and increased ID{4000007=apoptosis} , , may be the underlying basis how the ID{34000001=E50K} ID{2725=optineurin} mutation renders the ID{7000363=patients} predisposed to the ID{9010273=glaucoma} pathology.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:2725">
    <attr name="Name" value="OPTN" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-meshdis:Glaucoma">
    <attr name="Name" value="Glaucoma" />
    <attr name="NodeType" value="Disease" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="GeneticChange" />
    <attr name="mutation" value="E50K" />
    <attr name="PubMonth" value="Jul" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="12" />
    <attr name="Issue" value="7" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="TextRef" value="info:pmid/20634958#body:255" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="2902519" />
    <attr name="Volume" value="5" />
    <attr name="ChangeType" value="mutation" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PubYear" value="2010" />
    <attr name="PMID" value="20634958" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1233" msrc="Nagabhushana et al. in their report suggested that the ID{12105551=ubiquitin} binding domain of ID{2725=optineurin} is required for the formation of vesicular structures (foci), impairment of ID{7018=transferrin} uptake, and ID{4108219=cell death} .">
  <nodes>
   <node local_id="N1" urn="urn:agi-gocellproc:0008219">
    <attr name="Name" value="cell death" />
    <attr name="NodeType" value="CellProcess" />
   </node>
   <node local_id="N2" urn="urn:agi-protfc:ubiquitin">
    <attr name="Name" value="ubiquitin" />
    <attr name="NodeType" value="FunctionalClass" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="unknown" />
    <attr name="PubMonth" value="Jul" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="12" />
    <attr name="Issue" value="7" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="TextRef" value="info:pmid/20634958#body:234" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="2902519" />
    <attr name="Volume" value="5" />
    <attr name="TextMods" value="55: 'UBD ' -&gt; 'ubiquitin binding domain ' 170: 'Tf ' -&gt; 'transferrin '" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PubYear" value="2010" />
    <attr name="PMID" value="20634958" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1014" msrc="Recently, ID{2725=optineurin} has been shown to be a negative regulator of ID{12000005=NF-κB} .">
  <nodes>
   <node local_id="N1" urn="urn:agi-aopfc:0000156">
    <attr name="Name" value="NF-kB" />
    <attr name="NodeType" value="FunctionalClass" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:2725">
    <attr name="Name" value="OPTN" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="negative" />
    <attr name="PubMonth" value="Jul" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="12" />
    <attr name="Issue" value="7" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="TextRef" value="info:pmid/20634958#body:15" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="2902519" />
    <attr name="Volume" value="5" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PubYear" value="2010" />
    <attr name="PMID" value="20634958" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1016" msrc="ID{2725=Optineurin} has also been demonstrated to interact with ID{2971=transcription factor IIIA} , Rab8 , - , ID{3064=huntingtin} , ID{4646=myosin VI} , , ID{0,29110,12000140=metabotropic glutamate receptor , and TANK-binding kinase 1} .">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:2971">
    <attr name="Name" value="GTF3A" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:2725">
    <attr name="Name" value="OPTN" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="Binding" />
    <attr name="PubMonth" value="Jul" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="12" />
    <attr name="Issue" value="7" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="TextRef" value="info:pmid/20634958#body:17" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="2902519" />
    <attr name="Volume" value="5" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PubYear" value="2010" />
    <attr name="PMID" value="20634958" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1016" msrc="ID{2725=Optineurin} has also been demonstrated to interact with ID{2971=transcription factor IIIA} , Rab8 , - , ID{3064=huntingtin} , ID{4646=myosin VI} , , ID{0,29110,12000140=metabotropic glutamate receptor , and TANK-binding kinase 1} .">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:3064">
    <attr name="Name" value="HTT" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:2725">
    <attr name="Name" value="OPTN" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="Binding" />
    <attr name="PubMonth" value="Jul" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="12" />
    <attr name="Issue" value="7" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="TextRef" value="info:pmid/20634958#body:17" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="2902519" />
    <attr name="Volume" value="5" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PubYear" value="2010" />
    <attr name="PMID" value="20634958" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1016" msrc="ID{2725=Optineurin} has also been demonstrated to interact with ID{2971=transcription factor IIIA} , Rab8 , - , ID{3064=huntingtin} , ID{4646=myosin VI} , , ID{0,29110,12000140=metabotropic glutamate receptor , and TANK-binding kinase 1} .">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:4646">
    <attr name="Name" value="MYO6" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:2725">
    <attr name="Name" value="OPTN" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="Binding" />
    <attr name="PubMonth" value="Jul" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="12" />
    <attr name="Issue" value="7" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="TextRef" value="info:pmid/20634958#body:17" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="2902519" />
    <attr name="Volume" value="5" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PubYear" value="2010" />
    <attr name="PMID" value="20634958" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1016" msrc="ID{2725=Optineurin} has also been demonstrated to interact with ID{2971=transcription factor IIIA} , Rab8 , - , ID{3064=huntingtin} , ID{4646=myosin VI} , , ID{0,29110,12000140=metabotropic glutamate receptor , and TANK-binding kinase 1} .">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:29110">
    <attr name="Name" value="TBK1" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:2725">
    <attr name="Name" value="OPTN" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="Binding" />
    <attr name="PubMonth" value="Jul" />
    <attr name="PubDay" value="12" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="Issue" value="7" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="TextRef" value="info:pmid/20634958#body:17" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="Volume" value="5" />
    <attr name="PMC" value="2902519" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PMID" value="20634958" />
    <attr name="PubYear" value="2010" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1016" msrc="ID{2725=Optineurin} has also been demonstrated to interact with ID{2971=transcription factor IIIA} , Rab8 , - , ID{3064=huntingtin} , ID{4646=myosin VI} , , ID{0,29110,12000140=metabotropic glutamate receptor , and TANK-binding kinase 1} .">
  <nodes>
   <node local_id="N1" urn="urn:agi-protfc:mglur">
    <attr name="Name" value="mGluR" />
    <attr name="NodeType" value="FunctionalClass" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:2725">
    <attr name="Name" value="OPTN" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="Binding" />
    <attr name="PubMonth" value="Jul" />
    <attr name="PubDay" value="12" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="Issue" value="7" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="TextRef" value="info:pmid/20634958#body:17" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="Volume" value="5" />
    <attr name="PMC" value="2902519" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PMID" value="20634958" />
    <attr name="PubYear" value="2010" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1070" msrc="To determine whether the impairment of ID{7018=transferrin} uptake could be rescued by ID{2725=optineurin} binding proteins ID{0,4646,7037=myosin VI, Rab8, or transferrin receptor}, ID{10000000,8802497,10004161=retinal pigment epithelium ( ) and retinal ganglion cell}5 (data not shown) cells were co-transfected with pOPTNWT-GFP along with pMyoVI-EGFP, pRab8ID{34000001=Q67L}-EGFP, or pTfR-EGFP for 16 h.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:4646">
    <attr name="Name" value="MYO6" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:2725">
    <attr name="Name" value="OPTN" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="Binding" />
    <attr name="PubMonth" value="Jul" />
    <attr name="PubDay" value="12" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="Issue" value="7" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="TextRef" value="info:pmid/20634958#body:71" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="Volume" value="5" />
    <attr name="PMC" value="2902519" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="TextMods" value="39: 'Tf ' -&gt; 'transferrin ' 126: 'TfR' -&gt; 'transferrin receptor' 148: 'RPE ' -&gt; 'retinal pigment epithelium ' 183: 'RGC' -&gt; 'retinal ganglion cell'" />
    <attr name="PMID" value="20634958" />
    <attr name="PubYear" value="2010" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1070" msrc="To determine whether the impairment of ID{7018=transferrin} uptake could be rescued by ID{2725=optineurin} binding proteins ID{0,4646,7037=myosin VI, Rab8, or transferrin receptor}, ID{10000000,8802497,10004161=retinal pigment epithelium ( ) and retinal ganglion cell}5 (data not shown) cells were co-transfected with pOPTNWT-GFP along with pMyoVI-EGFP, pRab8ID{34000001=Q67L}-EGFP, or pTfR-EGFP for 16 h.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:7037">
    <attr name="Name" value="TFRC" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:2725">
    <attr name="Name" value="OPTN" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="Binding" />
    <attr name="PubMonth" value="Jul" />
    <attr name="PubDay" value="12" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="Issue" value="7" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="TextRef" value="info:pmid/20634958#body:71" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="Volume" value="5" />
    <attr name="PMC" value="2902519" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="TextMods" value="39: 'Tf ' -&gt; 'transferrin ' 126: 'TfR' -&gt; 'transferrin receptor' 148: 'RPE ' -&gt; 'retinal pigment epithelium ' 183: 'RGC' -&gt; 'retinal ganglion cell'" />
    <attr name="PMID" value="20634958" />
    <attr name="PubYear" value="2010" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1170" msrc="The inducible ID{10000000,10004161=retinal ganglion cell}5 cell lines were similarly used to provide quantitative assessment of the ID{7037=transferrin receptor} binding capability of wild type and mutant ID{2725=optineurin}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:2725">
    <attr name="Name" value="OPTN" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:7037">
    <attr name="Name" value="TFRC" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="Binding" />
    <attr name="PubMonth" value="Jul" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="12" />
    <attr name="Issue" value="7" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="TextRef" value="info:pmid/20634958#body:171" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="2902519" />
    <attr name="Volume" value="5" />
    <attr name="TextMods" value="14: 'RGC' -&gt; 'retinal ganglion cell' 110: 'TfR ' -&gt; 'transferrin receptor '" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PubYear" value="2010" />
    <attr name="PMID" value="20634958" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1223" msrc="Since the ID{12105551=ubiquitin} binding domain and the ID{4646=myosin VI} and hungtintin interaction sites all reside in proximity to the C-terminus of the ID{2725=optineurin} sequence , , neither the ID{4000000,4116567=ubiquitination} nor the aforementioned protein interactions should be impacted by the ID{34000001=E50K} or the ID{34000001=Leu157 to Ala} mutation.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:4646">
    <attr name="Name" value="MYO6" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-protfc:ubiquitin">
    <attr name="Name" value="ubiquitin" />
    <attr name="NodeType" value="FunctionalClass" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="Binding" />
    <attr name="mutation" value="E50K" />
    <attr name="PubMonth" value="Jul" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="12" />
    <attr name="Issue" value="7" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="TextRef" value="info:pmid/20634958#body:224" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="2902519" />
    <attr name="Volume" value="5" />
    <attr name="TextMods" value="274: 'L157A ' -&gt; 'Leu157 to Ala ' 35: '(UBD) ' -&gt; ''" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PubYear" value="2010" />
    <attr name="PMID" value="20634958" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1233" msrc="Nagabhushana et al. in their report suggested that the ID{12105551=ubiquitin} binding domain of ID{2725=optineurin} is required for the formation of vesicular structures (foci), impairment of ID{7018=transferrin} uptake, and ID{4108219=cell death} .">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:2725">
    <attr name="Name" value="OPTN" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-protfc:ubiquitin">
    <attr name="Name" value="ubiquitin" />
    <attr name="NodeType" value="FunctionalClass" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="Binding" />
    <attr name="PubMonth" value="Jul" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="12" />
    <attr name="Issue" value="7" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="TextRef" value="info:pmid/20634958#body:234" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="2902519" />
    <attr name="Volume" value="5" />
    <attr name="TextMods" value="55: 'UBD ' -&gt; 'ubiquitin binding domain ' 170: 'Tf ' -&gt; 'transferrin '" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PubYear" value="2010" />
    <attr name="PMID" value="20634958" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1236" msrc="This mutant, similar to the ID{12105551=ubiquitin}-binding deficient but ID{12003209=leucine zipper}-intact ID{34000001=D477N} mutant , does not cause much foci formation ( ) or induce ID{4000007=apoptosis} (data not shown) as was seen in the wild type and ID{34000001=E50K} ID{2725=optineurin} cases.">
  <nodes>
   <node local_id="N1" urn="urn:agi-protfc:ubiquitin">
    <attr name="Name" value="ubiquitin" />
    <attr name="NodeType" value="FunctionalClass" />
   </node>
   <node local_id="N2" urn="urn:agi-protfc:leucine%20zipper">
    <attr name="Name" value="leucine zipper" />
    <attr name="NodeType" value="FunctionalClass" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="in-out" ref="N1" />
    <link type="in-out" ref="N2" />
    <attr name="ControlType" value="Binding" />
    <attr name="mutation" value="D477N" />
    <attr name="PubMonth" value="Jul" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="12" />
    <attr name="Issue" value="7" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="TextRef" value="info:pmid/20634958#body:237" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="2902519" />
    <attr name="Volume" value="5" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PubYear" value="2010" />
    <attr name="PMID" value="20634958" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1241" msrc="As both the overexpression and the suppression of ID{2725=optineurin} cause a reduction in the ID{7018=transferrin} ID{4000000,4210816=internalization}, a role of ID{2725=optineurin} in this process is implicated.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:7018">
    <attr name="Name" value="TF" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:2725">
    <attr name="Name" value="OPTN" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="UnknownRegulation" />
    <attr name="Effect" value="negative" />
    <attr name="PubMonth" value="Jul" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="12" />
    <attr name="Issue" value="7" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="TextRef" value="info:pmid/20634958#body:242" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="2902519" />
    <attr name="Volume" value="5" />
    <attr name="TextMods" value="86: 'Tf ' -&gt; 'transferrin '" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PubYear" value="2010" />
    <attr name="PMID" value="20634958" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1220" msrc="Taken all the data together, we conclude that the inhibition of ID{7018=transferrin} uptake triggered by overexpressed wild type and ID{34000001=E50K} ID{2725=optineurin} is related to the interaction between ID{0,2725,7037=optineurin and transferrin receptor}, the recruitment and sequestration of ID{7037=transferrin receptor} to the ID{2725=optineurin} foci and the ensued attenuation of ID{7037=transferrin receptor} molecules on the ID{2000000,2009928=cell surface}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:7018">
    <attr name="Name" value="TF" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:2725">
    <attr name="Name" value="OPTN" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="MolTransport" />
    <attr name="Effect" value="positive" />
    <attr name="PubMonth" value="Jul" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="12" />
    <attr name="Issue" value="7" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="TextRef" value="info:pmid/20634958#body:221" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="2902519" />
    <attr name="Volume" value="5" />
    <attr name="TextMods" value="64: 'Tf ' -&gt; 'transferrin ' 193: 'TfR' -&gt; 'transferrin receptor' 252: 'TfR ' -&gt; 'transferrin receptor ' 326: 'TfR ' -&gt; 'transferrin receptor '" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PubYear" value="2010" />
    <attr name="PMID" value="20634958" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1220" msrc="Taken all the data together, we conclude that the inhibition of ID{7018=transferrin} uptake triggered by overexpressed wild type and ID{34000001=E50K} ID{2725=optineurin} is related to the interaction between ID{0,2725,7037=optineurin and transferrin receptor}, the recruitment and sequestration of ID{7037=transferrin receptor} to the ID{2725=optineurin} foci and the ensued attenuation of ID{7037=transferrin receptor} molecules on the ID{2000000,2009928=cell surface}.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:7018">
    <attr name="Name" value="TF" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:2725">
    <attr name="Name" value="OPTN" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="MolTransport" />
    <attr name="Effect" value="unknown" />
    <attr name="PubMonth" value="Jul" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="12" />
    <attr name="Issue" value="7" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="TextRef" value="info:pmid/20634958#body:221" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="2902519" />
    <attr name="Volume" value="5" />
    <attr name="TextMods" value="64: 'Tf ' -&gt; 'transferrin ' 193: 'TfR' -&gt; 'transferrin receptor' 252: 'TfR ' -&gt; 'transferrin receptor ' 326: 'TfR ' -&gt; 'transferrin receptor '" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PubYear" value="2010" />
    <attr name="PMID" value="20634958" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1249" msrc="Evidence is provided in the current study that the inhibition of ID{7018=transferrin} uptake by overexpressed ID{2725=optineurin} is mediated largely through the foci formation and the augmented ID{0,2725,7037=optineurin-transferrin receptor} interaction.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:7018">
    <attr name="Name" value="TF" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:2725">
    <attr name="Name" value="OPTN" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="MolTransport" />
    <attr name="Effect" value="unknown" />
    <attr name="PubMonth" value="Jul" />
    <attr name="PubDay" value="12" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="Issue" value="7" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="TextRef" value="info:pmid/20634958#body:250" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="Volume" value="5" />
    <attr name="PMC" value="2902519" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="TextMods" value="65: 'Tf ' -&gt; 'transferrin ' 188: 'TfR ' -&gt; 'transferrin receptor '" />
    <attr name="PMID" value="20634958" />
    <attr name="PubYear" value="2010" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1249" msrc="Evidence is provided in the current study that the inhibition of ID{7018=transferrin} uptake by overexpressed ID{2725=optineurin} is mediated largely through the foci formation and the augmented ID{0,2725,7037=optineurin-transferrin receptor} interaction.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:7018">
    <attr name="Name" value="TF" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:7037">
    <attr name="Name" value="TFRC" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="MolTransport" />
    <attr name="Effect" value="unknown" />
    <attr name="PubMonth" value="Jul" />
    <attr name="PubDay" value="12" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="Issue" value="7" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="TextRef" value="info:pmid/20634958#body:250" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="Volume" value="5" />
    <attr name="PMC" value="2902519" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="TextMods" value="65: 'Tf ' -&gt; 'transferrin ' 188: 'TfR ' -&gt; 'transferrin receptor '" />
    <attr name="PMID" value="20634958" />
    <attr name="PubYear" value="2010" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
 <resnet mref="20634958:1249" msrc="Evidence is provided in the current study that the inhibition of ID{7018=transferrin} uptake by overexpressed ID{2725=optineurin} is mediated largely through the foci formation and the augmented ID{0,2725,7037=optineurin-transferrin receptor} interaction.">
  <nodes>
   <node local_id="N1" urn="urn:agi-llid:7018">
    <attr name="Name" value="TF" />
    <attr name="NodeType" value="Protein" />
   </node>
   <node local_id="N2" urn="urn:agi-llid:2725">
    <attr name="Name" value="OPTN" />
    <attr name="NodeType" value="Protein" />
   </node>
  </nodes>
  <controls>
   <control local_id="L1">
    <link type="out" ref="N1" />
    <link type="in" ref="N2" />
    <attr name="ControlType" value="MolTransport" />
    <attr name="Effect" value="negative" />
    <attr name="PubMonth" value="Jul" />
    <attr name="ESSN" value="1932-6203" />
    <attr name="PubDay" value="12" />
    <attr name="Issue" value="7" />
    <attr name="DOI" value="10.1371/journal.pone.0011547" />
    <attr name="TextRef" value="info:pmid/20634958#body:250" />
    <attr name="Journal" value="PLoS ONE" />
    <attr name="MedlineTA" value="PLoS One" />
    <attr name="PMC" value="2902519" />
    <attr name="Volume" value="5" />
    <attr name="TextMods" value="65: 'Tf ' -&gt; 'transferrin ' 188: 'TfR ' -&gt; 'transferrin receptor '" />
    <attr name="Title" value="Impairment of Protein Trafficking upon Overexpression and Mutation of Optineurin" />
    <attr name="PubYear" value="2010" />
    <attr name="PMID" value="20634958" />
    <attr name="Authors" value="Park,B.C.;Ying,H.;Shen,X.;Park,J.S.;Qiu,Y.;Shyam,R.;Yue,B.Y.J.T." />
    <attr name="PubTypes" value="Journal Article" />
   </control>
  </controls>
 </resnet>
</batch>
