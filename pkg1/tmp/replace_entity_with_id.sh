#!/bin/bash

cp ras2_model_peter_CLEAN ras2_model_peter_CLEAN_copy

echo $(wc -l ras2_model_peter_CLEAN_copy)

nLines=$(wc -l ras2_id.txt | cut -f1 -d' ')

for i in $(seq 1 $nLines); do
    
    searchFor=$(sed -n "$i"p ras2_entity.txt)

    replaceWith=$(sed -n "$i"p ras2_id_database_info.txt)
    
    cat ras2_model_peter_CLEAN_copy | sed -e "s/${searchFor}(/${replaceWith}\(/I" > temp

	rm ras2_model_peter_CLEAN_copy

	mv temp ras2_model_peter_CLEAN_copy
 
    echo 'Finished line:' $i
    
done

	 
