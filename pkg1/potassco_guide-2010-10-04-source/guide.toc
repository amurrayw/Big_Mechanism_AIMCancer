\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{4}{section.1}
\contentsline {section}{\numberline {2}Quickstart}{6}{section.2}
\contentsline {subsection}{\numberline {2.1}Problem Instance}{6}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Problem Encoding}{7}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Problem Solution}{8}{subsection.2.3}
\contentsline {section}{\numberline {3}Input Languages}{9}{section.3}
\contentsline {subsection}{\numberline {3.1}Input Language of \texttt {gringo}\ and \texttt {clingo}}{9}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Normal Programs and Integrity Constraints}{9}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Classical Negation}{11}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Disjunction}{12}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}Built-In Arithmetic Functions}{12}{subsubsection.3.1.4}
\contentsline {subsubsection}{\numberline {3.1.5}Built-In Comparison Predicates}{13}{subsubsection.3.1.5}
\contentsline {subsubsection}{\numberline {3.1.6}Assignments}{14}{subsubsection.3.1.6}
\contentsline {subsubsection}{\numberline {3.1.7}Intervals}{14}{subsubsection.3.1.7}
\contentsline {subsubsection}{\numberline {3.1.8}Conditions}{16}{subsubsection.3.1.8}
\contentsline {subsubsection}{\numberline {3.1.9}Pooling}{17}{subsubsection.3.1.9}
\contentsline {subsubsection}{\numberline {3.1.10}Aggregates}{18}{subsubsection.3.1.10}
\contentsline {subsubsection}{\numberline {3.1.11}Optimization}{22}{subsubsection.3.1.11}
\contentsline {subsubsection}{\numberline {3.1.12}Meta-Statements}{23}{subsubsection.3.1.12}
\contentsline {paragraph}{Comments.}{23}{section*.4}
\contentsline {paragraph}{Hiding Predicates.}{23}{section*.5}
\contentsline {paragraph}{Constant Replacement.}{24}{section*.6}
\contentsline {paragraph}{Domain Declarations.}{24}{section*.7}
\contentsline {paragraph}{Compute Statements.}{24}{section*.8}
\contentsline {paragraph}{External Statements.}{25}{section*.9}
\contentsline {subsubsection}{\numberline {3.1.13}Integrated Scripting Language}{26}{subsubsection.3.1.13}
\contentsline {subsection}{\numberline {3.2}Input Language of \texttt {iclingo}}{29}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Input Language of \texttt {clasp}}{31}{subsection.3.3}
\contentsline {section}{\numberline {4}Examples}{32}{section.4}
\contentsline {subsection}{\numberline {4.1}$N$-Coloring}{32}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Problem Instance}{32}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Problem Encoding}{33}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Problem Solution}{33}{subsubsection.4.1.3}
\contentsline {subsection}{\numberline {4.2}Traveling Salesperson}{34}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Problem Instance}{34}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Problem Encoding}{35}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}Problem Solution}{36}{subsubsection.4.2.3}
\contentsline {subsection}{\numberline {4.3}Blocks-World Planning}{37}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Problem Instance}{37}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}Problem Encoding}{38}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3}Problem Solution}{39}{subsubsection.4.3.3}
\contentsline {section}{\numberline {5}Command Line Options}{40}{section.5}
\contentsline {subsection}{\numberline {5.1}\texttt {gringo}\ Options}{40}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}\texttt {clingo}\ Options}{41}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}\texttt {iclingo}\ Options}{42}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}\texttt {clasp}\ Options}{43}{subsection.5.4}
\contentsline {subsubsection}{\numberline {5.4.1}General Options}{43}{subsubsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.2}Search Options}{45}{subsubsection.5.4.2}
\contentsline {subsubsection}{\numberline {5.4.3}Lookback Options}{46}{subsubsection.5.4.3}
\contentsline {section}{\numberline {6}Errors and Warnings}{47}{section.6}
\contentsline {subsection}{\numberline {6.1}Errors}{47}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Warnings}{49}{subsection.6.2}
\contentsline {section}{\numberline {7}Future Work}{49}{section.7}
\contentsline {section}{References}{50}{section*.10}
\contentsline {section}{\numberline {A}Differences to the Language of \texttt {lparse}}{50}{appendix.A}
